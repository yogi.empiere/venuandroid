package com.venu.club;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.venu.club.Activity.HomeActivity;

import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

	@Override
	public void onNewToken(String s) {
		Log.e("NEW_TOKEN", s);
	}

	/*@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {

		Map<String, String> params = remoteMessage.getData();
		JSONObject object = new JSONObject(params);
		Log.e("JSON_OBJECT", object.toString());

		String NOTIFICATION_CHANNEL_ID = "VenuChannel";

		long pattern[] = {0, 1000, 500, 1000};

		NotificationManager mNotificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Your Notifications",
					NotificationManager.IMPORTANCE_HIGH);

			notificationChannel.setDescription("");
			notificationChannel.enableLights(true);
			notificationChannel.setLightColor(Color.BLACK);
			notificationChannel.setVibrationPattern(pattern);
			notificationChannel.enableVibration(true);
			mNotificationManager.createNotificationChannel(notificationChannel);
		}

		// to diaplay notification in DND Mode
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = mNotificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID);
			channel.canBypassDnd();
		}

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

		notificationBuilder.setAutoCancel(true)
				.setColor(ContextCompat.getColor(this, R.color.colorAccent))
				.setContentTitle(getString(R.string.app_name))
				.setContentText(remoteMessage.getNotification().getBody())
				.setDefaults(Notification.DEFAULT_ALL)
				.setWhen(System.currentTimeMillis())
				.setSmallIcon(R.drawable.ic_launcher_background)
				.setAutoCancel(true);


		mNotificationManager.notify(1000, notificationBuilder.build());


	}
*/
	public void onMessageReceived(RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);
		Log.d("msg", "onMessageReceived: " + remoteMessage.getData().get("message"));
		Intent intent = new Intent(this, HomeActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
		String channelId = "Default";
		NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
				.setSmallIcon(R.drawable.transprent_venu_logo)
				.setContentTitle(remoteMessage.getNotification().getTitle())
				.setContentText(remoteMessage.getNotification().getBody()).setAutoCancel(true).setContentIntent(pendingIntent);;
		NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
			manager.createNotificationChannel(channel);
		}
		manager.notify(0, builder.build());
	}

}