package com.venu.club.Fragment;


import android.graphics.Canvas;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.venu.club.Adapter.CurrentNotificationAdapter;
import com.venu.club.Model.CurrentNotificationModel;
import com.venu.club.R;
import com.venu.club.Utility.SwipeController;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentFragment extends Fragment {
    RecyclerView rvNotification;
    public RecyclerView.LayoutManager layoutManager;
    public CurrentNotificationAdapter currentNotificationAdapter;
    public ArrayList<CurrentNotificationModel> notificationModelArrayList;
    SwipeController swipeController = new SwipeController();
    ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);

    public CurrentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_current, container, false);
        bindWidgetReference(view);
        bindWidgetEvents();
        addData();

        return view;
    }

    private void addData() {
        notificationModelArrayList = new ArrayList<>();
        CurrentNotificationModel currentNotificationModel = new CurrentNotificationModel();

        for (int i = 0; i < 10; i++) {
            currentNotificationModel.setNotication("Your order is ready");
            currentNotificationModel.setNotificationTime("04.00 PM");
            notificationModelArrayList.add(currentNotificationModel);
        }
        setData();
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        rvNotification.setHasFixedSize(true);
        rvNotification.setLayoutManager(layoutManager);
        rvNotification.setItemViewCacheSize(2);
        currentNotificationAdapter = new CurrentNotificationAdapter(getContext(), notificationModelArrayList);
        rvNotification.setAdapter(currentNotificationAdapter);
        itemTouchhelper.attachToRecyclerView(rvNotification);
        rvNotification.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    private void bindWidgetEvents() {

    }

    private void bindWidgetReference(View view) {
        rvNotification = view.findViewById(R.id.rvNotification);
    }

}
