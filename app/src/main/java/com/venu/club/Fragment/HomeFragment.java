package com.venu.club.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.venu.club.Activity.HomeActivity;
import com.venu.club.Activity.LoginActivity;
import com.venu.club.Adapter.HomeAdapter;
import com.venu.club.App;
import com.venu.club.Model.VenuModel;
import com.venu.club.Model.VenueImages;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.venu.club.Activity.HomeActivity.IvSearch;
import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Activity.HomeActivity.TvItem;
import static com.venu.club.Activity.HomeActivity.ivmenu;
import static com.venu.club.Utility.MyConstants.DEFAULT_PAGE_RECORDS;
import static com.venu.club.Utility.MyConstants.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.venu.club.Utility.MySharedPref.USER_LATITUDE;
import static com.venu.club.Utility.MySharedPref.USER_LONGITUDE;
import static com.venu.club.Utility.Utils.checkGPS;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, LocationListener {

    RecyclerView rvHomeBar;
    public RecyclerView.LayoutManager layoutManager;
    public HomeAdapter homeAdapter;
    public ArrayList<VenuModel> venuArray;
    public ImageView ivBack;
    private GoogleApiClient mGoogleApiClient;
    public JsonParserUniversal jParser;
    public MySharedPref myShared;
    public SwipeRefreshLayout swipeContainer;
    public LinearLayout llNoSearchFound;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        myShared = new MySharedPref(getActivity().getApplicationContext());
        jParser = new JsonParserUniversal();
        venuArray = new ArrayList<>();
        // getLocation();
        bindWidgetEvents(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED)
                buildGoogleApiClient();
            else
                checkLocationPermission();
        } else
            buildGoogleApiClient();


        getCartData(0);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    Log.i("Venue", "Venue --> Home fragment on Back Pressed");
                    return true;
                }
                return false;
            }
        } );
        return view;
    }

    public void checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getCartData(0);
    }

    private synchronized void buildGoogleApiClient() {

        Log.i("TAG --->", "Home Fragment in Build google Api client");
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        System.out.println("google api client connected");


    }

    private void bindWidgetEvents(View view) {

        IvSearch.setVisibility(View.VISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        Ivcart.setVisibility(View.VISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        TvItem.setText(R.string.nearby_venue);
        rvHomeBar = view.findViewById(R.id.rvHomeBar);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        llNoSearchFound = view.findViewById(R.id.llNoSearchFound);


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNearByVenues(0);
            }
        });
        // Configure the refreshing colors
       /* swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);*/

        if (!myShared.isLogin()) {
            Ivcart.setVisibility(View.GONE);
        }

        swipeContainer.setColorSchemeResources(R.color.grey, R.color.grey_border, R.color.textcolor2, R.color.colorPrimaryDark);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.

        Log.i("TAG --->", "Home Fragment in Request permission result");

        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted. Do the
                // contacts-related task you need to do.
                System.out.println("granted before if");
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    buildGoogleApiClient();

                    return;
                }
            }

            Utils.showWarn(getActivity(), MyConstants.DECLINE_LOCATION_MESSAGE);
            new MySharedPref(getActivity().getApplicationContext()).sharedPrefClear();
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
            getActivity().finish();


            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }


    private void fetchNearByVenues(int page) {

        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("lat", String.valueOf(latitude));
        postData.put("lon", String.valueOf(longitude));

        postData.put("size", MyConstants.DEFAULT_PAGE_RECORDS);


        String URL = Utils.convertToGetURL(Services.GET_VENUE_LIST_BY_LOCATION, postData);

        new NetworkCall(getActivity(), URL, true, response -> {

            if (swipeContainer.isRefreshing())
                swipeContainer.setRefreshing(false);
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    venuArray = new ArrayList<>();

                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    JSONArray jContentArray = jsonData.getJSONArray("contentList");

                    for (int i = 0; i < jContentArray.length(); i++) {

                        JSONObject jVenuObject = jContentArray.getJSONObject(i);
                        VenuModel vm = (VenuModel) jParser.parseJson(jVenuObject, new VenuModel());
                        JSONArray jImagesArray = jVenuObject.getJSONArray("venueImages");

                        for (int j = 0; j < jImagesArray.length(); j++) {
                            JSONObject jImageObject = jImagesArray.getJSONObject(j);
                            VenueImages vi = (VenueImages) jParser.parseJson(jImageObject, new VenueImages());
                            vm.venuImagesArray.add(vi);
                        }
                        venuArray.add(vm);
                    }
                    setData();
                } else {
                    rvHomeBar.setVisibility(View.GONE);
                    llNoSearchFound.setVisibility(View.VISIBLE);

                }
            } catch (Exception e) {
                e.printStackTrace();
                // Utils.showWarn(getActivity(), MyConstants.COMMON_ERROR_MESSAGE);
                rvHomeBar.setVisibility(View.GONE);
                llNoSearchFound.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        System.out.println("on connected");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on suspended");
    }

    private double latitude = 0.0, longitude = 0.0;

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        location.setProvider(checkGPS(getActivity()) ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER);

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        MySharedPref mySharedPref = new MySharedPref(getActivity().getApplicationContext());
        mySharedPref.setString(USER_LATITUDE, String.valueOf(latitude));
        mySharedPref.setString(USER_LONGITUDE, String.valueOf(longitude));

        System.out.println("lat---->" + latitude + " long---->" + longitude);

        if (venuArray == null || venuArray.size() == 0) {
            fetchNearByVenues(0);
        }

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

        }
        Log.d("onLocationChanged", "Exit");
    }

    private void getLocation() {
        String ip = Utils.getIPAddress(true);
        new NetworkCall(getActivity(), "http://api.ipstack.com/" + ip + "?access_key=7dfce808d14a2500719ce4626383cb18&format=1",
                true, response -> {
            try {
                JSONObject jsonObject = new JSONObject(response);
                latitude = jsonObject.getDouble("latitude");
                longitude = jsonObject.getDouble("longitude");

                MySharedPref mySharedPref = new MySharedPref(getActivity().getApplicationContext());
                mySharedPref.setString(USER_LATITUDE, String.valueOf(latitude));
                mySharedPref.setString(USER_LONGITUDE, String.valueOf(longitude));

                System.out.println("API lat---->" + latitude + " long---->" + longitude);


                if (venuArray == null || venuArray.size() > 0) {
                    fetchNearByVenues(0);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void setData() {


        layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvHomeBar.setHasFixedSize(true);
        rvHomeBar.setLayoutManager(layoutManager);
        homeAdapter = new HomeAdapter(getContext(), venuArray);
        rvHomeBar.setAdapter(homeAdapter);

        if (venuArray.size() != 0) {
            rvHomeBar.setVisibility(View.VISIBLE);
            llNoSearchFound.setVisibility(View.GONE);
        } else {
            rvHomeBar.setVisibility(View.GONE);
            llNoSearchFound.setVisibility(View.VISIBLE);
        }
    }

    private void getCartData(int page) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("size", DEFAULT_PAGE_RECORDS);
        postData.put("userId", myShared.getUserId() + "");

        String URL = Utils.convertToGetURL(Services.GET_ALL_CART_ITEMS, postData);

        new NetworkCall(getActivity(), URL, true, response -> {
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    JSONObject jData = jsonObject.getJSONObject("data");

                    int noOFResults = jData.getInt("numberOfResults");

                    if (noOFResults >= 0) {
                        App.cartData = jData.getJSONArray("contentList");
                        App.cartCounter = App.cartData.length();

                        if (myShared.isLogin()) {
                            Utils.setCartBadge(Ivcart);
                            Ivcart.setVisibility(View.VISIBLE);
                        } else {
                            Ivcart.setVisibility(View.GONE);
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            if (venuArray == null || venuArray.size() == 0) {
                fetchNearByVenues(0);
            }
        });

    }






}
