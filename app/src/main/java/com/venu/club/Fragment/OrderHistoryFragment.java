package com.venu.club.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.venu.club.Activity.CardListActivity;
import com.venu.club.Activity.CartActivity;
import com.venu.club.Activity.SplitPaymentActivity;
import com.venu.club.Adapter.OrderHistoryExpandableAdapter;
import com.venu.club.App;
import com.venu.club.Interfaces.OrderHistoryListener;
import com.venu.club.Model.OrderHistory;
import com.venu.club.Model.OrderHistoryExpand;
import com.venu.club.Model.VenuModel;
import com.venu.club.Model.VenueImages;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;
import com.venu.club.viewHolder.OrderCollapseViewHolder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.venu.club.Activity.HomeActivity.IvSearch;
import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Activity.HomeActivity.TvItem;
import static com.venu.club.Activity.HomeActivity.ivmenu;
import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends Fragment implements OrderHistoryListener {

    public RecyclerView rvOrderList;
    public RelativeLayout relNoOrders;
    public RecyclerView.LayoutManager layoutManager;
    public OrderHistoryExpandableAdapter orderHistoryExpandableAdapter;
    public List<OrderHistory> orderHistoryArray;
    public JsonParserUniversal jParser;
    public MySharedPref myShared;

    public OrderHistoryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        setHasOptionsMenu(true);
        jParser = new JsonParserUniversal();
        myShared = new MySharedPref(getActivity().getApplicationContext());

        bindWidgetReference(view);
        bindWidgetEvents();
        getOrderHistory(0);
        return view;
    }

    private void bindWidgetReference(View view) {
        IvSearch.setVisibility(View.VISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        Ivcart.setVisibility(View.VISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        TvItem.setText("Order History");
        rvOrderList = view.findViewById(R.id.rvOrderList);
        relNoOrders = view.findViewById(R.id.relNoOrders);

        if(myShared.isLogin()){
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }


    }

    private void bindWidgetEvents() {

    }

    private void getOrderHistory(int page) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("size", MyConstants.DEFAULT_PAGE_RECORDS);
        postData.put("userId", String.valueOf(App.loggedInUser.userId));

        String URL = Utils.convertToGetURL(Services.LIST_USER_ORDER, postData);

        new NetworkCall(getActivity(), URL, true, response -> {
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    orderHistoryArray = new ArrayList<>();

                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    JSONArray jContentArray = jsonData.getJSONArray("contentList");

                    Gson gson = new Gson();
                    orderHistoryArray = gson.fromJson(jContentArray.toString(), new TypeToken<List<OrderHistory>>() {}.getType());

                    setData();
                }
            } catch (Exception e) {
                e.printStackTrace();
                //  Utils.showWarn(getActivity(), MyConstants.COMMON_ERROR_MESSAGE);
            }
        });
    }

    private List<OrderHistoryExpand> getOrderHistory() {
        List<OrderHistoryExpand> orderHistoryExpands = new ArrayList<>();

        for (int i = 0; i < orderHistoryArray.size(); i++) {
            String date = Utils.getDate(orderHistoryArray.get(i).getOrderDate()).split(",")[1].trim();
            if (!isTitleAvailable(orderHistoryExpands, date)) {
                System.out.println("---------- " + date);
                orderHistoryExpands.add(new OrderHistoryExpand(date, setMonthHeader(date)));
            }
        }

        return orderHistoryExpands;
    }

    private boolean isTitleAvailable(List<OrderHistoryExpand> orderHistoryExpands, String title) {
        for (int i = 0; i < orderHistoryExpands.size(); i++) {
            if (orderHistoryExpands.get(i).getTitle().equals(title)) {
                System.out.println("---------- return true");
                return true;
            }
        }
        return false;
    }

    private List<OrderHistory> setMonthHeader(String month) {
        List<OrderHistory> histories = new ArrayList<>();
        String previousMonth = null;

        for (OrderHistory orderModel : orderHistoryArray) {
            if (Utils.getDate(orderModel.getOrderDate()).split(",")[1].trim().equalsIgnoreCase(month)) {
                histories.add(orderModel);
            }
        }
        return histories;
    }


    /*private void setMonthHeader(ArrayList<OrderModel> list) {
        String prevMonth = null;

        orderList = new ArrayList<>();

        for (OrderModel orderModel : list) {
            String newMonth = orderModel.getCreatedAt().split("-")[1].trim();

            if (!newMonth.equalsIgnoreCase(prevMonth)) {
                OrderModel headerModel = new OrderModel();
                headerModel.setType(OrderAdapter.VIEW_TYPE_HEADER);
                headerModel.setCreatedAt(orderModel.getCreatedAt());
                orderList.add(headerModel);
            }

            orderModel.setType(OrderAdapter.VIEW_TYPE_ITEM);
            orderList.add(orderModel);

            prevMonth = newMonth;
        }
    }*/


    private void setData() {

        if (orderHistoryArray.size() == 0) {
            relNoOrders.setVisibility(View.VISIBLE);
            rvOrderList.setVisibility(View.GONE);
        } else {
            relNoOrders.setVisibility(View.GONE);
            rvOrderList.setVisibility(View.VISIBLE);
        }
        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rvOrderList.setHasFixedSize(true);
        rvOrderList.setLayoutManager(layoutManager);
        rvOrderList.setItemViewCacheSize(2);
        orderHistoryExpandableAdapter = new OrderHistoryExpandableAdapter(getOrderHistory(), this);

        rvOrderList.setAdapter(orderHistoryExpandableAdapter);

    }


    @Override
    public void onPayNowClick(OrderHistory orderHistory) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", myShared.getUserId().toString());
        postData.put("orderId", orderHistory.getOrderId() + "");
        postData.put("isSplit", "Yes");

/*
        $order_id = $request->get('orderId');
        $user_id = $request->get('userId');
        $card_id = $request->get('cardID');*/


        Intent i = new Intent(getActivity(), CardListActivity.class);
        i.putExtra(KEY_CART_PAYMENT, false);
        i.putExtra(KEY_CART_MAP, postData);
        startActivity(i);

    }
}
