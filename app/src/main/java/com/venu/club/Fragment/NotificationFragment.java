package com.venu.club.Fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.venu.club.Adapter.NotificationTabsPagerAdapter;
import com.venu.club.R;

import static com.venu.club.Activity.HomeActivity.IvSearch;
import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Activity.HomeActivity.TvItem;
import static com.venu.club.Activity.HomeActivity.ivmenu;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public NotificationTabsPagerAdapter adapter;
    Typeface typeface;


    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        setHasOptionsMenu(true);
        bindwidgetReferrence(view);
        bindWidgetEvents();
        return view;
    }

    private void bindwidgetReferrence(View view) {
        viewPager = view.findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tabs);
        IvSearch.setVisibility(View.INVISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        Ivcart.setVisibility(View.VISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        TvItem.setText("Notifications");

    }

    private void bindWidgetEvents() {
        tabLayout.addTab(tabLayout.newTab().setText("Current"));
        tabLayout.addTab(tabLayout.newTab().setText("Archived"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new NotificationTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {

            TabLayout.Tab  tab = tabLayout.getTabAt(i);
            if (tab != null) {

                TextView tabTextView = new TextView(getContext());
                tab.setCustomView(tabTextView);

                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.setText(tab.getText());
                // First tab is the selected tab, so if i==0 then set BOLD typeface
                if (i == 0) {
                    tabTextView.setTypeface(typeface, Typeface.BOLD);
                   // tabTextView.setAllCaps(true);
                }
            }
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                TextView text = (TextView) tab.getCustomView();

               text.setTypeface(typeface, Typeface.BOLD);
              //  text.setAllCaps(true);
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTypeface(typeface, Typeface.NORMAL);
              //  text.setAllCaps(false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}


