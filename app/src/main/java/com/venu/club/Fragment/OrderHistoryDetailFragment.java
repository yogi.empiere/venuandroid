package com.venu.club.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.venu.club.Activity.CardListActivity;
import com.venu.club.Activity.HomeActivity;
import com.venu.club.Activity.ReportOrderActivity;
import com.venu.club.App;
import com.venu.club.Controls.TextBold;
import com.venu.club.Controls.TextLight;
import com.venu.club.Controls.TextMedium;
import com.venu.club.Model.CartModel;
import com.venu.club.Model.OrderHistory;
import com.venu.club.Model.VenuAddons;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.venu.club.Activity.HomeActivity.IvSearch;
import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Activity.HomeActivity.TvItem;
import static com.venu.club.Activity.HomeActivity.ivmenu;
import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;
import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_SETTINGS;
import static com.venu.club.Utility.MyConstants.KEY_ORDER_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryDetailFragment extends Fragment {


    View view;
    public ArrayList<CartModel> cartModels;
    private TextMedium tvBarName;
    private TextView tvTime;
    private TextView tvStatus;

    private TextView tvSubTotal;
    private TextView tVenuFee;
    private TextView tvTotalPrice;
    public OrderHistory orderHistory;
    public JsonParserUniversal jParser;
    public LinearLayout linMainView, linReport;

    public RelativeLayout relPayNow;
    public TextView tvPayNow, tvSplitAmount;
    public MySharedPref myShared;
    public int isReported, UserId, is_cart_order, orderId;
    public String order_dt, price, venueName, time, isSplit, orderStatus, totalAmount, adminFee, amountTransferred, paymentStatus, splitAmount, splitPaymentStatus;


    public OrderHistoryDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_history_detail, container, false);
        setHasOptionsMenu(true);
        jParser = new JsonParserUniversal();
        myShared = new MySharedPref(getActivity().getApplicationContext());

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(MyConstants.KEY_ORDER_DETAIL_MODEL)) {
                orderHistory = (OrderHistory) bundle.getSerializable(MyConstants.KEY_ORDER_DETAIL_MODEL);

            }
        }
        initView();
        bindEvents();
        getData();
        return view;
    }


    private void bindEvents() {
        IvSearch.setVisibility(View.VISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        HomeActivity.Ivcart.setVisibility(View.VISIBLE);
        TvItem.setText("Order #000" + orderHistory.getOrderId());


        if (myShared.isLogin()) {
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }


    }


    private void getData() {

        HashMap<String, String> postData = new HashMap<>();
        // postData.put("userId", String.valueOf(App.loggedInUser.userId));
        postData.put("userId", myShared.getUserId().toString());
        postData.put("orderId", orderHistory.getOrderId() + "");

        String URL = Utils.convertToGetURL(Services.ORDER_DETAIL, postData);

        new NetworkCall(getActivity(), URL, true, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    JSONObject jsonData = jsonObject.getJSONObject("data");

                    orderId = jsonData.getInt("orderId");
                    venueName = jsonData.getString("venueName");
                    isSplit = jsonData.getString("isSplit");
                    is_cart_order = jsonData.getInt("is_cart_order");
                    orderStatus = jsonData.getString("orderStatus");
                    price = jsonData.getString("price");
                    totalAmount = jsonData.getString("totalAmount");
                    adminFee = String.valueOf(jsonData.getDouble("adminFee"));
                    amountTransferred = jsonData.getString("amountTransferred");
                    isReported = jsonData.getInt("isReported");
                    UserId = jsonData.getInt("UserId");
                    paymentStatus = jsonData.getString("paymentStatus");
                    splitAmount = jsonData.getString("splitAmount");
                    splitPaymentStatus = jsonData.getString("splitPaymentStatus");
                    order_dt = jsonData.getString("order_dt");


                    tvBarName.setText(venueName);
                    time = Utils.getDateInMinute(jsonData.getLong("orderDate"));
                    tvTime.setText(time);
                    tvStatus.setText(orderStatus);
                    // tvSubTotal.setText(orderStatus);
                    tvTotalPrice.setText("$" + Utils.convertToTwoDecimal(totalAmount));
                    tVenuFee.setText("$" + adminFee);
                    tvSubTotal.setText("$"+Utils.convertToTwoDecimal((Double.valueOf(totalAmount) - Double.valueOf(adminFee)) + ""));

                    JSONArray jCartItemsArray = jsonData.getJSONArray("items");

                    if (jCartItemsArray.length() > 0) {
                        cartModels = new ArrayList<>();

                        for (int i = 0; i < jCartItemsArray.length(); i++) {

                            JSONObject jCartObj = jCartItemsArray.getJSONObject(i);
                            CartModel cartModel = (CartModel) jParser.parseJson(jCartObj, new CartModel());

                            if (jCartObj.has("addons")) {
                                JSONArray jAddons = jCartObj.getJSONArray("addons");

                                if (jAddons.length() > 0) {
                                    for (int a = 0; a < jAddons.length(); a++) {
                                        VenuAddons vAddons = (VenuAddons) jParser.parseJson(jAddons.getJSONObject(a), new VenuAddons());
                                        cartModel.venuAddons.add(vAddons);
                                    }
                                }
                            }

                            cartModels.add(cartModel);
                        }

                        addMultipleItems(linMainView, cartModels);
                    }

                    if (!splitPaymentStatus.isEmpty() && splitPaymentStatus.equalsIgnoreCase("PENDING")) {
                        relPayNow.setVisibility(View.VISIBLE);

                        tvSplitAmount.setText("$" + splitAmount);

                        relPayNow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HashMap<String, String> postData = new HashMap<>();
                                postData.put("userId", myShared.getUserId().toString());
                                postData.put("orderId", orderId + "");
                                postData.put("isSplit", "Yes");


                                Intent i = new Intent(getActivity(), CardListActivity.class);
                                i.putExtra(KEY_CART_PAYMENT, false);
                                i.putExtra(KEY_CART_MAP, postData);
                                startActivity(i);
                            }
                        });
                    }

                    if (isReported == 1) {
                        linReport.setVisibility(View.GONE);
                    } else {
                        linReport.setVisibility(View.VISIBLE);
                        linReport.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(getContext(), ReportOrderActivity.class);
                                i.putExtra(KEY_ORDER_ID, orderId + "");
                                startActivity(i);

                            }
                        });
                    }


                }


            } catch (Exception e) {
                e.printStackTrace();
                // Utils.showWarn(getActivity(), MyConstants.COMMON_ERROR_MESSAGE);
            }
        });

    }


    private void initView() {
        tvBarName = view.findViewById(R.id.tvBarName);
        tvTime = view.findViewById(R.id.tvTime);
        tvStatus = view.findViewById(R.id.tvStatus);
        //  tvItemName = view.findViewById(R.id.tvItemName);
        //  tvItemPrice = view.findViewById(R.id.tvItemPrice);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tVenuFee = view.findViewById(R.id.tVenuFee);
        tvTotalPrice = view.findViewById(R.id.tvTotalPrice);
        linMainView = view.findViewById(R.id.linMainView);
        linReport = view.findViewById(R.id.linReport);
        relPayNow = view.findViewById(R.id.relPayNow);
        tvPayNow = view.findViewById(R.id.tvPayNow);
        tvSplitAmount = view.findViewById(R.id.tvSplitAmount);

    }

    public void addMultipleItems(LinearLayout mainLinearView, ArrayList<CartModel> cartModels) {


        mainLinearView.removeAllViews();
        mainLinearView.invalidate();

        for (CartModel ca : cartModels) {

            View mainView = LayoutInflater.from(getActivity()).inflate(
                    R.layout.order_detail_multiple_row, null);

            TextView tvItemName, tvItemPrice;

            tvItemName = mainView.findViewById(R.id.tvItemName);
            tvItemPrice = mainView.findViewById(R.id.tvItemPrice);


            tvItemName.setText(ca.itemName);
            tvItemPrice.setText("$" + Utils.convertToTwoDecimal(ca.price));

            LinearLayout linAddons = mainView.findViewById(R.id.linAddons);

            linAddons.removeAllViews();
            linAddons.invalidate();

            for (VenuAddons va : ca.venuAddons) {
                View addOnsView = LayoutInflater.from(getActivity()).inflate(
                        R.layout.cart_extra_addons, null);

                TextView tvAddonName, tvAddonPrice;

                addOnsView.findViewById(R.id.view11).setVisibility(View.GONE);
                tvAddonName = addOnsView.findViewById(R.id.tvAddonName);
                tvAddonPrice = addOnsView.findViewById(R.id.tvAddonPrice);

                tvAddonName.setTextSize(getResources()
                        .getDimensionPixelSize(R.dimen._4sdp));
                tvAddonPrice.setTextSize(getResources()
                        .getDimensionPixelSize(R.dimen._4sdp));

                tvAddonName.setText(va.getName());
                String adPrice = va.price;

                Double tempAddonPrice = 0.00;
                if (!adPrice.isEmpty() && !adPrice.equalsIgnoreCase("free")) {
                    tempAddonPrice += Double.valueOf(Utils.convertToTwoDecimal(adPrice));
                }

                //addonPrice += tempAddonPrice;

                //   tempAddonPrice = quauntityCount * tempAddonPrice;

                tvAddonPrice.setText(tempAddonPrice == 0.0 ? "Free" : "$" + Utils.convertToTwoDecimal(tempAddonPrice + ""));
                linAddons.addView(addOnsView);

            }


            mainLinearView.addView(mainView);
        }


    }


}
