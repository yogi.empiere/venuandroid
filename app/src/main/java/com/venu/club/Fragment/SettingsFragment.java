package com.venu.club.Fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Activity.EndUserLicenceActivity;
import com.venu.club.Activity.LoginActivity;
import com.venu.club.Activity.CardListActivity;
import com.venu.club.Activity.PrivacyPolicyActivity;
import com.venu.club.Activity.ProfileActivity;
import com.venu.club.App;
import com.venu.club.BuildConfig;
import com.venu.club.Controls.TextLight;
import com.venu.club.R;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.venu.club.Activity.HomeActivity.IvSearch;
import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Activity.HomeActivity.TvItem;
import static com.venu.club.Activity.HomeActivity.ivmenu;
import static com.venu.club.Utility.MyConstants.COMMON_ERROR_MESSAGE;
import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_SETTINGS;
import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_TERMS;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    private Dialog dialog;
    public LinearLayout llDisablenotifi, llProfile, llPaymentSetting, llPrivacy, llEndLicence, llShareApp;
    public TextView TvEndLicence, TvPrivacy, TvSignOut;
    public MySharedPref mShared;
    public ImageView ivCheck, ivUncheck;
    public int e_notifyStatus = 0;


    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        mShared = new MySharedPref(getActivity().getApplicationContext());
        bindWidgetReference(view);
        bindWidgetEvents();
        return view;
    }

    private void bindWidgetEvents() {
        llDisablenotifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotificationPopup();
            }
        });
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ProfileActivity.class);
                i.putExtra(KEY_IS_FROM_TERMS, false);
                startActivity(i);
            }
        });
        llPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), PrivacyPolicyActivity.class);
                startActivity(i);

            }
        });
        llPaymentSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), CardListActivity.class);
                i.putExtra(KEY_IS_FROM_SETTINGS, true);
                startActivity(i);

            }
        });
        llEndLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EndUserLicenceActivity.class);
                startActivity(i);

            }
        });
        llShareApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Venu App");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Choose Any"));
                } catch (Exception e) {
                    //e.toString();
                }

            }
        });

        TvSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();

            }
        });

    }


    public void updateNotificationSettings() {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", mShared.getUserId().toString());
        postData.put("status", e_notifyStatus == 0 ? "1" : "0");


        new NetworkCall(getActivity(), Services.ENABLE_NOTIFICATION, postData,
                true, response -> {
            try {


                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {

                    e_notifyStatus = (e_notifyStatus == 0) ? 1 : 0;
                    mShared.setInt(mShared.E_NOTIFY, e_notifyStatus);
                    ivCheck.setVisibility(e_notifyStatus == 0 ? View.GONE : View.VISIBLE);
                    ivUncheck.setVisibility(e_notifyStatus == 0 ? View.VISIBLE : View.GONE);
                    Utils.showWarn(getActivity(), "Notification settings updated successfully", 2, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                } else {
                    Utils.showToast(getActivity(), COMMON_ERROR_MESSAGE);
                }


            } catch (Exception e) {
                e.printStackTrace();
                //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });
    }

    private void signOut() {
        dialog = new Dialog(getContext(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_signout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llcancel = dialog.findViewById(R.id.llcancel);
        LinearLayout llSignOut = dialog.findViewById(R.id.llSignOut);
        llSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLoginInfo();
                Intent i = new Intent(getContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                getActivity().finish();
                dialog.dismiss();
            }
        });
        llcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    public void clearLoginInfo() {
        App.loggedInUser = null;
        Utils.cancelAlarm(getActivity());

        mShared.sharedPrefClear();
    }

    private void bindWidgetReference(View view) {
        IvSearch.setVisibility(View.INVISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        Ivcart.setVisibility(View.VISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        TvItem.setText("Settings");

        llDisablenotifi = view.findViewById(R.id.llDisablenotifi);
        TvPrivacy = view.findViewById(R.id.TvPrivacy);
        TvEndLicence = view.findViewById(R.id.TvEndLicence);
        TvSignOut = view.findViewById(R.id.TvSignOut);
        llProfile = view.findViewById(R.id.llProfile);
        llPaymentSetting = view.findViewById(R.id.llPaymentSetting);
        llPrivacy = view.findViewById(R.id.llPrivacy);
        llEndLicence = view.findViewById(R.id.llEndLicence);
        llShareApp = view.findViewById(R.id.llShareApp);
        ivCheck = view.findViewById(R.id.ivCheck);
        ivUncheck = view.findViewById(R.id.ivUncheck);

        e_notifyStatus = mShared.getInt(mShared.E_NOTIFY);

        ivCheck.setVisibility(e_notifyStatus == 0 ? View.GONE : View.VISIBLE);
        ivUncheck.setVisibility(e_notifyStatus == 0 ? View.VISIBLE : View.GONE);
    }

    private void showNotificationPopup() {
        dialog = new Dialog(getContext(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_disable_notification);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llCancel = dialog.findViewById(R.id.llCancel);
        LinearLayout llDisable = dialog.findViewById(R.id.llDisable);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText((e_notifyStatus == 0) ? R.string.enaable_notifications_title : R.string.disable_notifications_title);
        ((TextLight) llDisable.findViewById(R.id.tvLbl)).
                setText((e_notifyStatus == 0) ? "ENABLE" : "DISABLE");
        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        llDisable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNotificationSettings();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
