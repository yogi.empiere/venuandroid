package com.venu.club.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CartModel implements Serializable {

    /*
     "temp_cart_id": 178,
             "itemName": "Bentspoke",
             "quantity": 2,
             "add_ons": "119,2278,2277",
             "add_on_price": "8.000,0.000,0.000",
             "price": "9.000",
             "menu_item_id": 14,
             "brand_name": "No Brand",
             "venuID": 3,
             "brand_price": "9.000",*/

    public int temp_cart_id, quantity, menu_item_id, venuID;
    public String itemName;
    public String add_ons;

    public ArrayList<VenuAddons> venuAddons = new ArrayList<>();

    public int getTemp_cart_id() {
        return temp_cart_id;
    }

    public void setTemp_cart_id(int temp_cart_id) {
        this.temp_cart_id = temp_cart_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(int menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public int getVenuID() {
        return venuID;
    }

    public void setVenuID(int venuID) {
        this.venuID = venuID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getAdd_ons() {
        return add_ons;
    }

    public void setAdd_ons(String add_ons) {
        this.add_ons = add_ons;
    }

    public String getAdd_on_price() {
        return add_on_price;
    }

    public void setAdd_on_price(String add_on_price) {
        this.add_on_price = add_on_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_price() {
        return brand_price;
    }

    public void setBrand_price(String brand_price) {
        this.brand_price = brand_price;
    }

    public String add_on_price;
    public String price;
    public String brand_name;
    public String brand_price;
}
