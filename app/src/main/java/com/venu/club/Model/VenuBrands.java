package com.venu.club.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VenuBrands implements Serializable {


    /*"brandId": 16,
                                        "isHide": 0,
                                        "brandName": "Rothbury Estate Cuvee",
                                        "price": "8.000",
                                        "venue_item_id": 58,
                                        "addOns": []*/

    public int brandId, isHide, venue_item_id;
    public String brandName, price;
    public ArrayList<VenuAddons> addonsArray = new ArrayList<>();

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getIsHide() {
        return isHide;
    }

    public void setIsHide(int isHide) {
        this.isHide = isHide;
    }

    public int getVenue_item_id() {
        return venue_item_id;
    }

    public void setVenue_item_id(int venue_item_id) {
        this.venue_item_id = venue_item_id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ArrayList<VenuAddons> getAddonsArray() {
        return addonsArray;
    }

    public void setAddonsArray(ArrayList<VenuAddons> addonsArray) {
        this.addonsArray = addonsArray;
    }
}
