package com.venu.club.Model;

import java.io.Serializable;

/* "img_id": 11,
          "venue_id": 3,
          "image_name": "",
          "image_url": "\/uploads\/images\/venu\/treehouse.jpeg",
          "doc_url": null,
          "is_active": 1,
          "created_on": "2019-04-08 23:32:20",
          "created_at": "2019-04-08 18:02:20",
          "updated_at": "2019-04-08 18:02:20",
          "modified_on": "2019-04-08 23:32:20",
          "imageId": 11,
          "name": "",
          "imageUrl": "\/uploads\/images\/venu\/treehouse.jpeg"*/
public class VenueImages implements Serializable {

    public int img_id,venue_id,imageId,is_active;

    public String image_name;
    public String image_url;
    public String doc_url;
    public String created_on;
    public String created_at;

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDoc_url() {
        return doc_url;
    }

    public void setDoc_url(String doc_url) {
        this.doc_url = doc_url;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String updated_at;
    public String modified_on;
    public String name;
    public String imageUrl;


}
