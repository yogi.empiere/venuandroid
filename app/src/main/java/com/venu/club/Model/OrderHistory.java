package com.venu.club.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class OrderHistory implements Serializable, Parcelable {

    public int orderId, quantity, isReported, UserId, order_dt;
    public long orderDate;
    public double adminFee;

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }

    public OrderHistory(Parcel in) {
        orderId = in.readInt();
        quantity = in.readInt();
        isReported = in.readInt();
        UserId = in.readInt();
        order_dt = in.readInt();
        adminFee = in.readDouble();
        isSplit = in.readString();
        paymentStatus = in.readString();
        splitAmount = in.readString();
        splitPaymentStatus = in.readString();
        itemName = in.readString();
        venueName = in.readString();
        orderStatus = in.readString();
        price = in.readString();
        totalAmount = in.readString();
    }

    public static final Creator<OrderHistory> CREATOR = new Creator<OrderHistory>() {
        @Override
        public OrderHistory createFromParcel(Parcel in) {
            return new OrderHistory(in);
        }

        @Override
        public OrderHistory[] newArray(int size) {
            return new OrderHistory[size];
        }
    };

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsReported() {
        return isReported;
    }

    public void setIsReported(int isReported) {
        this.isReported = isReported;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getOrder_dt() {
        return order_dt;
    }

    public void setOrder_dt(int order_dt) {
        this.order_dt = order_dt;
    }

    public double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(double adminFee) {
        this.adminFee = adminFee;
    }

    public String getIsSplit() {
        return isSplit;
    }

    public void setIsSplit(String isSplit) {
        this.isSplit = isSplit;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getSplitAmount() {
        return splitAmount;
    }

    public void setSplitAmount(String splitAmount) {
        this.splitAmount = splitAmount;
    }

    public String getSplitPaymentStatus() {
        return splitPaymentStatus;
    }

    public void setSplitPaymentStatus(String splitPaymentStatus) {
        this.splitPaymentStatus = splitPaymentStatus;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }



    public String isSplit, paymentStatus, splitAmount, splitPaymentStatus, itemName, venueName, orderStatus, price, totalAmount;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(orderId);
        dest.writeInt(quantity);
        dest.writeInt(isReported);
        dest.writeInt(UserId);
        dest.writeInt(order_dt);
        dest.writeDouble(adminFee);
        dest.writeString(isSplit);
        dest.writeString(paymentStatus);
        dest.writeString(splitAmount);
        dest.writeString(splitPaymentStatus);
        dest.writeString(itemName);
        dest.writeString(venueName);
        dest.writeString(orderStatus);
        dest.writeString(price);
        dest.writeString(totalAmount);
    }


   /* orderId: 78,
    isSplit: "Yes",
    paymentStatus: "PENDING",
    splitAmount: "29.40",
    splitPaymentStatus: "PENDING",
    itemName: "Gin",
    quantity: 1,
    venueName: "Treehouse Bar",
    orderStatus: "RECEIVED",
    price: "10.000",
    adminFee: 4.2,
    totalAmount: "88.20",
    isReported: 0,
    UserId: 3,
    order_dt: 1558335379,
    orderDate: "1558335379"*/
}
