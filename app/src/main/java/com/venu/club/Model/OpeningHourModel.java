package com.venu.club.Model;

import java.io.Serializable;

public class OpeningHourModel implements Serializable {

    public String Days = "", Time = "", text = "";

    public String getDays() {
        return Days;
    }

    public void setDays(String days) {
        Days = days;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
