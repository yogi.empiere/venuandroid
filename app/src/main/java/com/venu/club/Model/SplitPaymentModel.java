package com.venu.club.Model;

import java.io.Serializable;

public class SplitPaymentModel implements Serializable {
    public String user_Id;
    public String user_Profile;
    public String userName, price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(String user_Id) {
        this.user_Id = user_Id;
    }

    public String getUser_Profile() {
        return user_Profile;
    }

    public void setUser_Profile(String user_Profile) {
        this.user_Profile = user_Profile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
