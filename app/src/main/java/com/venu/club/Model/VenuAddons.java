package com.venu.club.Model;

import java.io.Serializable;

public class VenuAddons implements Serializable {

    /*
    id: 2694,
name: "Extra Shot",
price: "4.000",
is_active: 1,
created_on: "2019-06-22 04:00:00",
modified_on: "2019-06-22 04:00:00"

    */

    public int id, is_active;
    public String name, price, created_on, modified_on;
    public boolean isSelected = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }
}
