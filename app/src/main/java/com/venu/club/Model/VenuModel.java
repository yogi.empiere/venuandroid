package com.venu.club.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VenuModel implements Serializable {

    public int venue_owner_id, stripe_varified, is_active, venueId;
    public double distance;
    public String add_ons, name, address, lat, lon, profit_sharing_amount,
            profit_sharing_expiry_date, placeID, created_on, modified_on;

    public ArrayList<VenueImages> venuImagesArray = new ArrayList<>();

    public int getVenue_owner_id() {
        return venue_owner_id;
    }

    public void setVenue_owner_id(int venue_owner_id) {
        this.venue_owner_id = venue_owner_id;
    }

    public int getStripe_varified() {
        return stripe_varified;
    }

    public void setStripe_varified(int stripe_varified) {
        this.stripe_varified = stripe_varified;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getAdd_ons() {
        return add_ons;
    }

    public void setAdd_ons(String add_ons) {
        this.add_ons = add_ons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getProfit_sharing_amount() {
        return profit_sharing_amount;
    }

    public void setProfit_sharing_amount(String profit_sharing_amount) {
        this.profit_sharing_amount = profit_sharing_amount;
    }

    public String getProfit_sharing_expiry_date() {
        return profit_sharing_expiry_date;
    }

    public void setProfit_sharing_expiry_date(String profit_sharing_expiry_date) {
        this.profit_sharing_expiry_date = profit_sharing_expiry_date;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public ArrayList<VenueImages> getVenuImagesArray() {
        return venuImagesArray;
    }

    public void setVenuImagesArray(ArrayList<VenueImages> venuImagesArray) {
        this.venuImagesArray = venuImagesArray;
    }


}
