package com.venu.club.Model;

import java.io.Serializable;

public class MenuItemListModel implements Serializable {

    public String sub_Product_Id, sub_Product_Name;

    public String getSub_Product_Id() {
        return sub_Product_Id;
    }

    public void setSub_Product_Id(String sub_Product_Id) {
        this.sub_Product_Id = sub_Product_Id;
    }

    public String getSub_Product_Name() {
        return sub_Product_Name;
    }

    public void setSub_Product_Name(String sub_Product_Name) {
        this.sub_Product_Name = sub_Product_Name;
    }
}
