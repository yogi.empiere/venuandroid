package com.venu.club.Model;

import java.io.Serializable;

public class SplitFriendModel implements Serializable {

    public String firstName,lastName,email,imageUrl,social_type,price = "";
    public int userID;
    public String social_id;
    public boolean isSelected = false;


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSocial_type() {
        return social_type;
    }

    public void setSocial_type(String social_type) {
        this.social_type = social_type;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    /*  {
        "userID": 1,
            "firstName": "Gabriel",
            "lastName": "Simard",
            "email": "simardgabriel@me.com",
            "imageUrl": "http://live-api.venuapp.club/images/customer/profilePic/https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10154591305317594&height=400&width=400&ext=1556200921&hash=AeTjuo_F9K4J0BIf",
            "social_type": "Facebook",
            "social_id": "10154591305317594"
    },*/

}
