package com.venu.club.Model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class OrderHistoryExpand extends ExpandableGroup<OrderHistory> {

    public OrderHistoryExpand(String title, List<OrderHistory> items) {
        super(title, items);
    }
}