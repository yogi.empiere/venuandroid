package com.venu.club.Model;

public class CardModel {

     /* "id": 10,
              "cardId": "card_1EcBmoGt2vFDoM9N2xQho3G7",
              "cardType": "Visa",
              "cardLast4digit": "4242"*/

     public String id, cardId, cardType, cardLast4digit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardLast4digit() {
        return cardLast4digit;
    }

    public void setCardLast4digit(String cardLast4digit) {
        this.cardLast4digit = cardLast4digit;
    }
}
