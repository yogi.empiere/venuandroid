package com.venu.club.Model;

import java.io.Serializable;

public class MenuListModel implements Serializable {
    public String product_id, Product_Name;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }
}
