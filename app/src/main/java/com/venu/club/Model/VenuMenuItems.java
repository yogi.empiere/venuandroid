package com.venu.club.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VenuMenuItems implements Serializable {

/*

  "name": "Glass of Sparkling Wine",
                                "brands": [
*/

    public String name;


    public ArrayList<VenuBrands> brandsArray = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<VenuBrands> getBrandsArray() {
        return brandsArray;
    }

    public void setBrandsArray(ArrayList<VenuBrands> brandsArray) {
        this.brandsArray = brandsArray;
    }
}
