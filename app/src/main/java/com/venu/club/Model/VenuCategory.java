package com.venu.club.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VenuCategory implements Serializable {

/*
    "category_id": 14,
            "venue_id": 3,
            "category_name": "Spirits",
            "is_active": 1,
            "display-order": 0,
            "created_on": "2019-04-08 20:20:32",
            "modified_on": "2019-04-08 20:20:32",
            "selected_nights": "1,2,3,4,5,6,7",
            "start_time": null,
            "end_time": null,
*/

    public int category_id,venue_id,is_active,display_order;
    public String category_name,created_on,modified_on,selected_nights,start_time;

    public boolean isOpened = false;

    public ArrayList<VenuMenuItems> menuItemsArray = new ArrayList<>();

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(int display_order) {
        this.display_order = display_order;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getSelected_nights() {
        return selected_nights;
    }

    public void setSelected_nights(String selected_nights) {
        this.selected_nights = selected_nights;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }
}
