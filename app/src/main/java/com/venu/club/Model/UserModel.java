package com.venu.club.Model;


import java.io.Serializable;


public class UserModel implements Serializable {
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getThumbImageUrl() {
        return thumbImageUrl;
    }

    public void setThumbImageUrl(String thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public boolean isFirstTimeLogin() {
        return isFirstTimeLogin;
    }

    public void setFirstTimeLogin(boolean firstTimeLogin) {
        isFirstTimeLogin = firstTimeLogin;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String email;
    public String firstName;
    public String lastName;
    public String thumbImageUrl;
    public String imageUrl;
    public String accessToken;
    public int e_notify= 1;
    public boolean isFirstTimeLogin = false;
    public int userId = 0;


}
/*    "userId": 0,
             "name": "Sagar Sojitra",
             "email": "sojitrasagar108@gmail.com",
             "firstName": "Sagar",
             "lastName": "Sojitra",
             "thumbImageUrl": "https://graph.facebook.com/2792496697446298/picture?type=large",
             "imageUrl": "https://graph.facebook.com/2792496697446298/picture?type=large",
             "isFirstTimeLogin": true,
             "accessToken": "79deLtUzhaiOn8Bj"*/