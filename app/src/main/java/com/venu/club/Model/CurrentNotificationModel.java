package com.venu.club.Model;

import java.io.Serializable;

public class CurrentNotificationModel implements Serializable {
    public String notication, notificationTime;
    public String getNotication() {
        return notication;
    }

    public void setNotication(String notication) {
        this.notication = notication;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

}
