package com.venu.club.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;

import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.venu.club.App;
import com.venu.club.BuildConfig;
import com.venu.club.R;
import com.venu.club.receiver.ReminderReceiver;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ru.nikartm.support.ImageBadgeView;

import static android.content.Context.WIFI_SERVICE;
import static com.venu.club.Utility.MyConstants.VENU_FEE_PERCENTAGE;

@SuppressWarnings("ConstantConditions")
public class Utils {


    public static DecimalFormat towDecimalFormat = new DecimalFormat("#.00");


    public static String DEVICE_ID = "";

    public static File googleFILE = null;


    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    private static ProgressDialog progressDialog;
    // public static KProgressHUD progressDialog;

    public static void showPopup(Context context) {
       /* progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
*/

        try {
            if (progressDialog == null) {
//                progressDialog = KProgressHUD.create(context)
//                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                        .setLabel("Please wait")
//                        .setCancellable(new DialogInterface.OnCancelListener() {
//                            @Override
//                            public void onCancel(DialogInterface
//                                                         dialogInterface) {
//                                Toast.makeText(context, "Please try again later!", Toast
//                                        .LENGTH_SHORT).show();
//                            }
//                        });

                progressDialog = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(false);
                progressDialog.show();

            }


            if (progressDialog != null) {
                progressDialog.show();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closePopup() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    public static void closeKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            ((Activity) context).getWindow().setSoftInputMode(WindowManager
                    .LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("EEEE, dd MMM yyyy", cal).toString();
        return date;
    }


    public static String getDateInMinute(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("hh:mm a", cal).toString();
        return date;
    }

    public static int randomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256),
                rnd.nextInt(256), rnd.nextInt(256));
    }

    public static String getDateFormat(String input, String output, String mydate) {
        SimpleDateFormat srcDf = new SimpleDateFormat(input, Locale.getDefault());
        try {
            Date date = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat(output, Locale.getDefault());
            mydate = destDf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mydate;
    }

    public static String getCurrentTime(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public static LatLng getLatLngFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            return new LatLng(location.getLatitude(), location.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAddressFromLatLng(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            int addLines = addresses.get(0).getMaxAddressLineIndex();
            if (addLines == 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                //city_name=city;
                return city + ", " + state;
                //return (address + ", " + city + ", " + state + ", " + country + "-" + postalCode);
            } else {
                StringBuilder address = new StringBuilder();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                for (int i = 0; i <= addLines; i++) {
                    address.append(addresses.get(0).getAddressLine(i));
                    //city_name=addresses.get(0).getLocality();
                    if (i != addresses.get(0).getMaxAddressLineIndex())
                        address.append(", ");
                }
                return city + ", " + state;
                //return address.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public static String getNameFromLatLng(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            int addLines = addresses.get(0).getMaxAddressLineIndex();
            if (addLines == 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                //city_name=city;
                //return (address + ", " + city + ", " + state + ", " + country + "-" + postalCode);
                return addresses.get(0).getFeatureName();
            } else {
                StringBuilder address = new StringBuilder();
                for (int i = 0; i <= addLines; i++) {
                    address.append(addresses.get(0).getAddressLine(i));
                    //city_name=addresses.get(0).getLocality();
                    if (i != addresses.get(0).getMaxAddressLineIndex())
                        address.append(", ");
                }
                //return address.toString();
                return addresses.get(0).getFeatureName();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isInvalidEmail(CharSequence target) {
        return target == null || !Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showWarn(Context context, String msg) {
        showWarn(context, msg, 3);
    }

    public static void showWarn(Context context, String msg, int type) {
        String title;
        int t;
        switch (type) {
            case 0:
                t = SweetAlertDialog.NORMAL_TYPE;
                title = "Information";
                break;
            case 1:
                t = SweetAlertDialog.ERROR_TYPE;
                title = "Error";
                break;
            case 2:
                t = SweetAlertDialog.SUCCESS_TYPE;
                title = "Success";
                break;
            default://3
                t = SweetAlertDialog.WARNING_TYPE;
                title = "Warning";
                break;
        }


        new SweetAlertDialog(context, t)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("OK")
                .setConfirmClickListener(SweetAlertDialog::dismissWithAnimation)
                .show();
    }

    public static void showWarn(Context context, String msg, int type, SweetAlertDialog.OnSweetClickListener listener) {
        String title;
        int t;
        switch (type) {
            case 0:
                t = SweetAlertDialog.NORMAL_TYPE;
                title = "Information";
                break;
            case 1:
                t = SweetAlertDialog.ERROR_TYPE;
                title = "Error";
                break;
            case 2:
                t = SweetAlertDialog.SUCCESS_TYPE;
                title = "Success";
                break;
            default://3
                t = SweetAlertDialog.WARNING_TYPE;
                title = "Warning";
                break;
        }


        new SweetAlertDialog(context, t)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("OK")
                .setConfirmClickListener(listener)
                .show();
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {

        return Settings.Secure.getString(context.
                getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    public static String getDeviceToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0)
            return "";
        char first = s.charAt(0);
        if (Character.isUpperCase(first))
            return s;
        else
            return Character.toUpperCase(first) + s.substring(1);
    }


    public static Bitmap getBitmapFromView(View view) {
        try {
            Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                    view.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            view.draw(c);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getIP(Context context) {
        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
    }

    //Check the internet connection.
    @SuppressWarnings("deprecation")
    public static String NetworkDetect(Context context) {

        boolean WIFI = false;

        boolean MOBILE = false;

        ConnectivityManager CM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert CM != null;
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();

        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected()) {
                    WIFI = true;
                    break;
                }

            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected()) {
                    MOBILE = true;
                    break;
                }
        }

        if (WIFI)
            return GetDeviceipWiFiData(context);
        if (MOBILE)
            return GetDeviceipMobileData();

        return "0.0.0.0";

    }


    private static String GetDeviceipMobileData() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface networkinterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.i("Current IP", ex.toString());
        }
        return null;
    }

    private static String GetDeviceipWiFiData(Context context) {

        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        assert wm != null;
        @SuppressWarnings("deprecation")
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {

        if (BuildConfig.DEBUG) {
            return "45.63.26.170";
        }
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    public static String convertToGetURL(String mainURL, HashMap<String, String> params) {

        String appendedParams = mainURL + "?";

        for (HashMap.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            appendedParams = appendedParams + key + "=" + value + "&";

        }

        if (appendedParams.endsWith("&")) {
            return appendedParams.substring(0, appendedParams.length() - 1);
        }
        return appendedParams;


    }

    public static boolean isDebug(Context ct) {
        return (0 != (ct.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));

    }

    public static int getCurrentDayOfWeek() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

    public static String convertToAmPM(String time) {

        String convertedTime = "";

        try {

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HHmm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(time);
            System.out.println(_24HourDt);
            convertedTime = _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedTime;
    }

    public static String convertToTwoDecimal(String value) {
        try {
            return towDecimalFormat.format(Double.valueOf(value));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return value;
        }
    }


    public static Double convertToTwoDecimal(Double value) {
        try {
            return Double.valueOf(towDecimalFormat.format(value));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return value;
        }
    }

    public static Double calculateVenuFee(Double value) {

        Double venuFee = value * VENU_FEE_PERCENTAGE;
        DecimalFormat towDecimalFormat = new DecimalFormat("0.00");
        return Double.valueOf(towDecimalFormat.format(venuFee));

    }


    public static String addExtraZero(Double value) {
        String strValue = String.valueOf(value);

        if(strValue.endsWith(".00")){
            return  strValue;
        }

        if (!strValue.endsWith("0")) {

            String sub = strValue.substring(strValue.indexOf("."), strValue.length());

            if(sub.length() == 2){
                return strValue + "0";
            }
          //  return  String.format("%.2f", strValue);

        }

        return strValue;
    }

    public static void setCartBadge(ImageBadgeView ivcart) {

        Log.i("TAG --->", "Cart Counter == " + App.cartCounter);

        if (App.cartCounter == 0) {
            ivcart.setBadgeValue(App.cartCounter);
            ivcart.manager.setBadgeVisibility(false);
            ivcart.invalidate();

        } else {
            ivcart.setBadgeValue(App.cartCounter);
        }
    }


    public static String getStripePublishableKey(Context context) {
       /* if (isDebug(context)) {
            return context.getString(R.string.test_publishable_key);
        }*/
        return context.getString(R.string.live_publishable_key);
    }


    public static String getStripeSecretKey(Context context) {
     /*   if (isDebug(context)) {
            return context.getString(R.string.test_secret_key);
        }*/
        return context.getString(R.string.live_secret_key);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void scheduleAlarm(Context context) {


        Intent intent = new Intent(context, ReminderReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 12345, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, 120000, pendingIntent);
        System.out.println("scheduleAlarm");
    }

    public static boolean checkAlaramSecheudled(Context context) {
        Intent intent = new Intent(context, ReminderReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 12345, intent, PendingIntent.FLAG_NO_CREATE);

        return pendingIntent != null;

    }

    public static void cancelAlarm(Context context) {
        Intent intent = new Intent(context, ReminderReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 12345, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }


    public static Target picassoImageTarget(Context context, final String imageDir, final String imageName, ImageView ivImage) {
        Log.d("picassoImageTarget", " picassoImageTarget");
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE); // path to /data/data/yourapp/app_imageDir
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            ivImage.setImageBitmap(bitmap);
                            ivImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }


            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {
                }
            }
        };
    }

    public static boolean checkGPS(Context ct) {
        try {
            LocationManager lm = (LocationManager)
                    ct.getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            try {
                assert lm != null;
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return gps_enabled;
        } catch (Exception e) {
            return false;
        }

    }

    public static void getImageAfterDownloaded(Context context, final String imageDir, final String imageName, ImageView ivImage) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File myImageFile = new File(directory, "my_image.jpeg");
        Picasso.get().load(myImageFile).into(ivImage);
    }
}
