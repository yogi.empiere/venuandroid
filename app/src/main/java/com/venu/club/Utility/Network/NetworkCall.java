package com.venu.club.Utility.Network;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.WindowManager;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.koushikdutta.async.http.AsyncSSLSocketMiddleware;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;


import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@SuppressWarnings("deprecation")
public class NetworkCall {

    private Context context;
    //private ProgressDialog progressDialog;
    private KProgressHUD progressDialog;
    private String url, req_type;
    private HashMap<String, File> postFiles;
    private HashMap<String, String> postData;
    private MultiPartRequest request;
    private Boolean isShow;

    public NetworkCall(Context context, String url,
                       HashMap<String, String> postData,
                       HashMap<String, File> postFiles,
                       Boolean isShow,
                       MultiPartRequest request) {
        this.context = context;
        this.url = url;
        this.postFiles = postFiles;
        this.postData = postData;
        this.request = request;
        this.isShow = isShow;
        this.req_type = "POST";
        if (isNetworkAvailable()) {
            if (isShow)
                showPopup(this.context);

            sendData();
        } else
            Utils.showWarn(context, "Please check your internet connection", 1);
    }

    public NetworkCall(Context context, String url,
                       HashMap<String, String> postData,
                       Boolean isShow,
                       MultiPartRequest request) {
        this.context = context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postData = postData;
        this.request = request;
        this.isShow = isShow;
        this.req_type = "POST";
        if (isNetworkAvailable()) {
            if (isShow)
                showPopup(this.context);

            sendData();
        } else
            Utils.showWarn(context, "Please check your internet connection", 1);
    }

    public NetworkCall(Context context, String url,
                       Boolean isShow,
                       MultiPartRequest request) {
        this.context = context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postData = new HashMap<>();
        this.request = request;
        this.isShow = isShow;
        this.req_type = "GET";
        if (isNetworkAvailable()) {
            if (isShow) {
                showPopup(context);
            }

            sendData();
        } else
            Utils.showWarn(context, "Please check your internet connection", 1);
    }

    private boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity == null) {
                closePopup();
                return false;
            } else {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED)
                        return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            closePopup();
        }

        return false;
    }

    private void sendData() {

        url = url.replaceAll(" ", "%20");

        Log.i("Url==>", url);
        List<Part> data = new ArrayList<>();
        for (String key : postData.keySet()) {
            String val = postData.get(key);
            data.add(new StringPart(key,
                    val == null ? "" : val));

            Log.i("postData", key + "==>" + val);
        }

        List<Part> files = new ArrayList<>();
        for (String key : postFiles.keySet()) {
            File val = postFiles.get(key);
            if (val != null) {
                files.add(new FilePart(key, val));

                Log.i("postFiles", key + "==>" + val.getAbsolutePath());
            }
        }

        //System.out.println("auth===="+new MySharedPref(context).getString(MySharedPref.USER_AUTH, ""));
        // Log.i("Auth===>",new MySharedPref(context).getString(MySharedPref.USER_AUTH, ""));


        setData(data, files);

    }

    private void setData(List<Part> data, List<Part> files) {

        try {

            TrustManager[] wrappedTrustManagers = new TrustManager[]{
                    new X509TrustManager() {
                        public void checkClientTrusted(X509Certificate[] chain, String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] chain, String authType) {
                        }

                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[]{};
                        }
                    }
            };

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, wrappedTrustManagers, null);

            AsyncSSLSocketMiddleware sslMiddleWare = Ion.getDefault(context).getHttpClient().getSSLSocketMiddleware();
            sslMiddleWare.setTrustManagers(wrappedTrustManagers);
            sslMiddleWare.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            sslMiddleWare.setSSLContext(sslContext);


            Builders.Any.B ion = Ion.with(context).load(req_type, url);
            if (files.size() != 0 && data.size() != 0)
                ion.addMultipartParts(files).addMultipartParts(data);
            else if (files.size() != 0)
                ion.addMultipartParts(files);
            else if (data.size() != 0)
                ion.addMultipartParts(data);


            ion.asString().setCallback((e, result) -> {
                try {
                    Log.i("response ==>", result + "_");
                    if(result == null){
                        request.myResponseMethod("");
                        closePopup();
                    }
                    request.myResponseMethod(result);
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("error_code")) {
                        String error_code = jsonObject.getString("error_code");
                        if (error_code.equals("-1"))
                            logout();
                    }

                    closePopup();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    request.myResponseMethod(e1.getLocalizedMessage());
                    closePopup();
                }
            });
        } catch (Exception e1) {
            e1.printStackTrace();
            request.myResponseMethod(e1.getLocalizedMessage());
            closePopup();
        }
    }


    public interface MultiPartRequest {
        void myResponseMethod(String response);
    }


    private void logout() {
        closePopup();
    }

    private void showPopup(Context context) {
        /*if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(true);
        }*/

        if (progressDialog == null) {
            progressDialog = new KProgressHUD(context);
            progressDialog
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }

        if (progressDialog != null && !progressDialog.isShowing()) {
            try {
                progressDialog.show();
                System.out.println("--------------- Call");
            } catch (WindowManager.BadTokenException | IllegalStateException e) {
                System.out.println("--------------- Exception");
                e.printStackTrace();
            }
        }


    }

    private void closePopup() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
