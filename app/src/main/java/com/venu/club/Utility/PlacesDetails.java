package com.venu.club.Utility;

import android.os.StrictMode;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PlacesDetails {
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_DETAIL = "/details";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyDklwNt01XU9QiNQTYrwNBX7k2DhZpSC_c";


    public PlacesDetails() {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public JSONObject placeDetail(String input) {
        ArrayList<Double> resultList = null;
        JSONObject result = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAIL + OUT_JSON);
            sb.append("?placeid=" + URLEncoder.encode(input, "utf8"));
            sb.append("&key=" + API_KEY);
            URL url = new URL(sb.toString());
            //Log.e("url", url.toString());
            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);

            }
            //System.out.println("le json result"+jsonResults.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return result;
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            //Log.e("creation du fichier Json", "creation du fichier Json");
            System.out.println("fabrication du Json Objet");
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            //JSONArray predsJsonArray = jsonObj.getJSONArray("html_attributions");
            result = jsonObj.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
            //System.out.println("la chaine Json "+result);
            Double longitude  = result.getDouble("lng");
            Double latitude =  result.getDouble("lat");
            //System.out.println("longitude et latitude "+ longitude+latitude);
            resultList = new ArrayList<Double>(result.length());
            resultList.add(result.getDouble("lng"));
            resultList.add(result.getDouble("lat"));
            //System.out.println("les latitude dans le table"+resultList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }


    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        /*PlacesDetails pl = new PlacesDetails();
        ArrayList<Double> list = new ArrayList<Double>();
        list = pl.placeDetail("ChIJ49XqJV2uEmsRPsTAF7eOlGg");
        System.out.println("resultat de la requette"+list.toString());*/
    }    
}
