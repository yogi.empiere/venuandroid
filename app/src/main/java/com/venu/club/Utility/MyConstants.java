package com.venu.club.Utility;

public class MyConstants {

    public static String USER_GROUP_ID = "3";
    public static String OWNER_GROUP_ID = "2";




    public static String COMMON_ERROR_MESSAGE ="We are unable to process your request, Please try again later.";
    public static String DECLINE_LOCATION_MESSAGE ="Oops!,We need location Permission to get near by Clubs for you!";




    public static String DEFAULT_PAGE_RECORDS = "10";



    public final  static int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public final static double VENU_FEE_PERCENTAGE = 0.05;


    /*Iamge directory*/
    public static String VENU_IMAGE_DIRECTORY = "Venu";

    /*All Keys*/

    public final static String KEY_VENU_MODEL ="venu_model";
    public final static String KEY_BRAND_MODEL ="brand_model";
    public final static String KEY_ADDON_MODEL ="addon_model";
    public final static String KEY_MENU_ITEM ="venu_menu_item";
    public final static String KEY_VENU_CATEGORY_ID ="venu_category_id";
    public final static String KEY_VENU_BRAND_ID ="venu_brand_id";
    public final static String KEY_VENU_ADDON_ID ="venu_addon_id";
    public final static String KEY_VENU_MENU_ITEM_ID ="menu_item_id";
    public final static String KEY_TOTAL_PRICE ="total_price";
    public final static String KEY_ORDER_DETAIL_MODEL ="order_detail_model";
    public final static String KEY_CART_PAYMENT ="cart_payment";
    public final static String KEY_CART_MAP ="cart_payment_map";
    public final static String KEY_IS_FROM_SETTINGS ="is_from_setting";
    public final static String KEY_INTENT_TRANSFER ="is_from_setting";
    public final static String KEY_CARD_ID ="card_id";
    public final static String KEY_ORDER_ID ="order_ID";
    public final static String KEY_ORDER_HISTORY_REDIRECT ="do_order_history_redirect";
    public final static String KEY_MENU_ITEM_NAME ="menu_item_name";




    public final static String KEY_IS_FROM_TERMS ="isFromTerms";

}
