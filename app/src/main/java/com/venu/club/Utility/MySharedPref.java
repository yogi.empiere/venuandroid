package com.venu.club.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPref {

    public static String USER_ID = "userId";
    public static String USER_ACCESS_TOKEN = "accessToken";
    public static String USER_F_NAME = "firstName";
    public static String USER_L_NAME = "lastName";
    public static String USER_EMAIL = "email";
    public static String USER_PROFILE_IMAGE = "imageUrl";
    public static String USER_IS_FIRST_TIME = "isFirstTimeLogin";
    public static String USER_LATITUDE = "latitude";
    public static String USER_LONGITUDE = "longitude";
    public static String LAST_UPDATE_TIME = "last_update_time";
    public static String IS_ALARM_SET = "is_alarm_set";
    public static String E_NOTIFY = "e_notify";


    public static String USER_NOTI = "isNotificationEnable";
    public static String USER_AGE = "Age";
    public static String USER_GENDER = "Gender";

    private String MY_PREF_NAME;
    private Context context;

    public MySharedPref(Context context) {
        super();
        this.context = context;
        MY_PREF_NAME = "venu_android";
    }

    public String getString(final String key, final String value) {
        SharedPreferences sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0);
        return sha.getString(key, value);
    }

    public long getLong(final String key, final long value) {
        SharedPreferences sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0);
        if (sha.contains(key)) {
            return sha.getLong(key, value);
        }
        return 0;
    }

    public boolean getBoolean(final String key) {
        SharedPreferences sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0);
        if (sha.contains(key)) {
            return sha.getBoolean(key, false);
        }
        return false;

    }

    public int getInt(final String key) {
        SharedPreferences sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0);

        if (sha.contains(key)) {
            return sha.getInt(key, 0);
        }
        return 0;
    }

    public void sharedPrefClear() {
        SharedPreferences.Editor sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.clear();
        sha.apply();
        sha.commit();
    }

    public boolean isLogin() {
        return !new MySharedPref(context)
                .getString(MySharedPref.USER_ACCESS_TOKEN, "").equals("");
    }

    public void setString(final String key, final String value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putString(key, value);
        sha.apply();
        sha.commit();
    }

    public void setInt(final String key, final int value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putInt(key, value);
        sha.apply();
    }


    public void setLong(final String key, final long value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putLong(key, value);
        sha.apply();
    }


    public void setBoolean(final String key, final boolean value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putBoolean(key, value);
        sha.apply();
    }

    public String getProfileImageURL() {
        return new MySharedPref(context)
                .getString(MySharedPref.USER_PROFILE_IMAGE, "");
    }

    public String getLatitude() {
        return new MySharedPref(context)
                .getString(MySharedPref.USER_LATITUDE, "");
    }

    public String getLongitude() {
        return new MySharedPref(context)
                .getString(MySharedPref.USER_LONGITUDE, "");
    }

    public String getUserAccessToken() {
        return new MySharedPref(context)
                .getString(MySharedPref.USER_ACCESS_TOKEN, "");
    }

    public Integer getUserId() {
        return new MySharedPref(context)
                .getInt(MySharedPref.USER_ID);
    }


}
