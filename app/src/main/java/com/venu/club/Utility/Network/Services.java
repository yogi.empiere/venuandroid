package com.venu.club.Utility.Network;


import com.venu.club.BuildConfig;

public class Services {
    public static boolean isDEBUG = BuildConfig.DEBUG;
    //public static boolean isDEBUG = false;

    public static String BASE_IMAGE_URL = "https://live-api.venuapp.club/";
    private static String BASE_URL = "https://live-api.venuapp.club/api/v1/customer/";

    /*public static String BASE_IMAGE_URL = "http://staging.venuapp.club/";
    private static String BASE_URL = "http://staging.venuapp.club/api/v1/customer/";*/


    public static String MAP_BASE_URL = "https://maps.googleapis.com/maps/api/place/details/json";

    public static String SOCIAL_LOGIN = BASE_URL + "signIn";
    public static String GET_VENUE_LIST_BY_LOCATION = BASE_URL + "getVenuListByLocation";
    public static String GET_MENU_DETAIL = BASE_URL + "getMenuDetail";
    public static String GET_ALL_CART_ITEMS = BASE_URL + "listUserCart";
    public static String ADD_TO_CART = BASE_URL + "addToCart";
    public static String UPDATE_CART = BASE_URL + "updateCart";
    public static String LIST_USER_ORDER = BASE_URL + "listUserOrders";
    public static String ORDER_DETAIL = BASE_URL + "getOrderDetail";
    public static String SPLIT_USER_FRIEND_LIST = BASE_URL + "getAllUser";
    public static String UPDATE_PROFILE = BASE_URL + "editProfile";
    public static String ADD_CARD = BASE_URL + "addCard";
    public static String GET_USER_CARDS = BASE_URL + "getCardList";
    public static String DELETE_CARD = BASE_URL + "deleteCard";
    public static String CHARGE_CUSTOMER = BASE_URL + "chargeCustomer";
    public static String CHARGE_CUSTOMER_CART = BASE_URL + "chargeCustomerCart";
    public static String CHARGE_SPLIT_CUSTOMER = BASE_URL + "chargeSplitCustomer";
    public static String VENUES_NEAR_ME = BASE_URL + "venuesNearMe";
    public static String REPORT_ORDER = BASE_URL + "reportItem";
    public static String TERMS_AND_COND =  BASE_URL + "terms_iphone";
    public static String PRIVACY_POLICY =  BASE_URL + "privacy_iphone";
    public static String ENABLE_NOTIFICATION =  BASE_URL + "enableNotification";



}
