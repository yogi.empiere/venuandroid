package com.venu.club.Interfaces;

import com.venu.club.Model.OrderHistory;

public interface OrderHistoryListener {

    public void onPayNowClick(OrderHistory orderHistory);
}
