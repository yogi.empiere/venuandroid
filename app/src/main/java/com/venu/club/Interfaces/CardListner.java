package com.venu.club.Interfaces;

public interface CardListner {
    public void onCardSelect(int pos);
    public void onDeleteCard(int pos);
}
