package com.venu.club.Interfaces;

public interface BrandAddonsListner {

    public void onAddonClick(int brandPos, int addonPos);
}
