package com.venu.club.Interfaces;

import com.venu.club.Model.SplitFriendModel;

public interface SplitFriendListner {
    public void addFriend(int pos, SplitFriendModel splitFriendModel);
    public void removeFriend(int pos, SplitFriendModel splitFriendModel);
}
