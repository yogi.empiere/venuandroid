package com.venu.club.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.venu.club.Activity.CardListActivity;
import com.venu.club.Activity.CartActivity;
import com.venu.club.Activity.HomeActivity;
import com.venu.club.Activity.SplitPaymentActivity;
import com.venu.club.Fragment.OrderHistoryDetailFragment;
import com.venu.club.Interfaces.OrderHistoryListener;
import com.venu.club.Model.OrderHistory;
import com.venu.club.Model.OrderHistoryExpand;
import com.venu.club.R;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.Utils;
import com.venu.club.viewHolder.OrderCollapseViewHolder;
import com.venu.club.viewHolder.OrderExpandViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;

public class OrderHistoryExpandableAdapter extends ExpandableRecyclerViewAdapter<OrderCollapseViewHolder, OrderExpandViewHolder> {


    public OrderHistoryListener oListner;
    public Fragment context;
    public ArrayList<OrderHistory> orderHistoryArrayList;

    public OrderHistoryExpandableAdapter(List<? extends ExpandableGroup> groups, Fragment context) {
        super(groups);
        this.context = context;
        this.oListner = (OrderHistoryListener) context;
    }


    public OrderHistoryExpandableAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public OrderCollapseViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_collapse, parent, false);

        OrderCollapseViewHolder holder = new OrderCollapseViewHolder(view);


        return holder;
    }

    @Override
    public OrderExpandViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        return new OrderExpandViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(OrderExpandViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final OrderHistory orderHistory = ((OrderHistoryExpand) group).getItems().get(childIndex);
        holder.TvBarid.setText("#000" + orderHistory.getOrderId() + "");
        holder.TvStatus.setText(orderHistory.getOrderStatus());
        holder.TvBarTitle.setText(orderHistory.getVenueName());


        if (orderHistory.splitPaymentStatus != null && orderHistory.splitPaymentStatus.equalsIgnoreCase("PENDING")) {

            holder.relPayNow.setVisibility(View.VISIBLE);
            holder.TvStatus.setVisibility(View.GONE);

            if (orderHistory.isSplit.equals("Yes")) {
                holder.tvSplitAmount.setText("$" + orderHistory.splitAmount);
            }

            holder.relPayNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    oListner.onPayNowClick(orderHistory);
                }
            });
        } else {
            holder.relPayNow.setVisibility(View.GONE);
            holder.TvStatus.setVisibility(View.VISIBLE);

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderHistoryDetailFragment orderHistoryDetailFragment = new OrderHistoryDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(MyConstants.KEY_ORDER_DETAIL_MODEL, orderHistory);

                orderHistoryDetailFragment.setArguments(bundle);
                HomeActivity.replaceFragment(orderHistoryDetailFragment, true);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(OrderCollapseViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.TvDate.setText(group.getTitle());

        holder.expand();


    }
}