package com.venu.club.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Activity.AddToCartItemActivity;
import com.venu.club.Activity.BrandAddonsActivity;
import com.venu.club.Model.VenuMenuItems;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;

import java.util.ArrayList;

import static com.venu.club.Utility.MyConstants.KEY_BRAND_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM_NAME;
import static com.venu.club.Utility.MyConstants.KEY_VENU_CATEGORY_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MENU_ITEM_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;

public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<VenuMenuItems> vMenuItemArray;
    public  VenuModel selectedVenu;

    public MenuItemAdapter(Context context, ArrayList<VenuMenuItems> vMenuItemArray,  VenuModel selectedVenu) {
        this.context = context;
        this.vMenuItemArray = vMenuItemArray;
        this.selectedVenu  = selectedVenu;
    }


    @Override
    public MenuItemAdapter.MyViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_item_list, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( MenuItemAdapter.MyViewHolder myViewHolder, int position) {
        VenuMenuItems vMenuItem = vMenuItemArray.get(position);
        myViewHolder.tvItemName.setText(vMenuItem.getName());
        myViewHolder.llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(vMenuItem.brandsArray.size() > 0) {
                    Intent i = new Intent(context, BrandAddonsActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra(KEY_MENU_ITEM, vMenuItem);
                    i.putExtra(KEY_VENU_MODEL, selectedVenu);
                    context.getApplicationContext().startActivity(i);
                } else {
                    Intent i = new Intent(context, AddToCartItemActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra(KEY_MENU_ITEM_NAME, vMenuItem.getName());
                    i.putExtra(KEY_VENU_MODEL, selectedVenu);

                    //  i.putExtra(KEY_BRAND_MODEL, vb);
                    //      i.putExtra(KEY_ADDON_MODEL, venuBrandsArray.get(venuBrandPos).addonsArray.get(venuAddonPos));
                    context.getApplicationContext().startActivity(i);


                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return vMenuItemArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName;
        LinearLayout llMenu;

        public MyViewHolder( View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            llMenu = itemView.findViewById(R.id.LLMenu);
        }
    }
}
