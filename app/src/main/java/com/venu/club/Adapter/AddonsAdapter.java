package com.venu.club.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.venu.club.Interfaces.BrandAddonsListner;
import com.venu.club.Model.VenuAddons;
import com.venu.club.R;

import java.util.ArrayList;

import static com.venu.club.Utility.Utils.convertToTwoDecimal;

public class AddonsAdapter extends RecyclerView.Adapter<AddonsAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<VenuAddons> vAddonsArray;
    public int venuCatId = 0, brandPos = 0;
    public BrandAddonsListner bLister;


    public AddonsAdapter(Context context, ArrayList<VenuAddons> vAddonsArray, int venuCatId, int brandPos) {
        this.context = context;
        this.vAddonsArray = vAddonsArray;
        this.venuCatId = venuCatId;
        this.brandPos = brandPos;
        this.bLister = (BrandAddonsListner) context;
    }


    @Override
    public AddonsAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_brand_addons, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddonsAdapter.MyViewHolder myViewHolder, int position) {
        VenuAddons venuAddons = vAddonsArray.get(position);
        myViewHolder.tvItemName.setText(venuAddons.getName());


        myViewHolder.tvPrice.setText(venuAddons.getPrice().startsWith("0.0") ? "Free" : "$" + convertToTwoDecimal(venuAddons.getPrice()));

        myViewHolder.chkRow.setChecked(venuAddons.isSelected);

        myViewHolder.llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {/*
                Intent i = new Intent(context, BrandAddonsActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(KEY_MENU_ITEM, venuAddons);
                i.putExtra(KEY_VENU_CATEGORY_ID, venuMenuItemID);
                context.getApplicationContext().startActivity(i);*/

                checkMultiple(position);
            }
        });


    }



    public void checkMultiple(int pos) {

        VenuAddons va  =  vAddonsArray.get(pos);

        va.isSelected = !va.isSelected;
        notifyDataSetChanged();

        bLister.onAddonClick(this.brandPos, pos);
    }

    @Override
    public int getItemCount() {
        return vAddonsArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName;
        public TextView tvPrice;
        public CheckBox chkRow;
        RelativeLayout llMenu;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            chkRow = itemView.findViewById(R.id.chkRow);
            llMenu = itemView.findViewById(R.id.LLMenu);
        }
    }
}
