package com.venu.club.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import androidx.fragment.app.FragmentStatePagerAdapter;

import com.venu.club.Fragment.ArchivedFragment;
import com.venu.club.Fragment.CurrentFragment;


public class NotificationTabsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public NotificationTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                CurrentFragment tab1 = new CurrentFragment();
                return tab1;
            case 1:
                ArchivedFragment tab2 = new ArchivedFragment();
                return tab2;
        }
        return null;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mNumOfTabs; //No of Tabs
    }


}
