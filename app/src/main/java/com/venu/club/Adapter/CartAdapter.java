package com.venu.club.Adapter;

import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Interfaces.CartListner;
import com.venu.club.Model.CartModel;
import com.venu.club.Model.VenuAddons;
import com.venu.club.R;
import com.venu.club.Utility.Utils;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    Context context;
    ArrayList<CartModel> cartModels;
    int count = 0;
    double newPrice = 0.0;
    private Dialog dialog;
    CartListner cartListner;


    public void setCartListner(CartListner cartListner) {
        this.cartListner = cartListner;
    }

    public CartAdapter(Context context, ArrayList<CartModel> cartModels) {
        this.context = context;
        this.cartModels = cartModels;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_cart_item, viewGroup, false);

        return new CartAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {
        CartModel cartModel = cartModels.get(position);

        String itemName = cartModel.getItemName();

        if (!cartModel.getBrand_name().equalsIgnoreCase("no brand")) {
            itemName = itemName + " (" + cartModel.getBrand_name() + ")";
        }
        myViewHolder.TvItemName.setText(itemName);
        myViewHolder.TvItemCount.setText(String.valueOf(cartModel.getQuantity()));
        myViewHolder.TvPrice.setText("$" + Utils.convertToTwoDecimal(cartModel.getPrice()));

        myViewHolder.IvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = cartModels.get(position).getQuantity() + 1;

                cartListner.changeSubTotal(position, newPrice, count);
            }
        });

        myViewHolder.IvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("src", "Decreasing value...");
                count = cartModels.get(position).getQuantity();
                if (count > 0) {
                    --count;

                    cartListner.changeSubTotal(position, newPrice, count);

                } else {
                    Log.d("src", "Value can't be less than 0");
                }
            }
        });


        myViewHolder.IvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItemDialogue(position);
            }
        });

        addMultipleAddons(myViewHolder.linAddons, cartModel.venuAddons);

        if (position == cartModels.size() - 1) {
            myViewHolder.view11.setVisibility(View.GONE);
        }

    }

    public void addMultipleAddons(LinearLayout linAddons, ArrayList<VenuAddons> venuAddonsArray) {


        linAddons.removeAllViews();
        linAddons.invalidate();

        for (VenuAddons va : venuAddonsArray) {

            View addOnsView = LayoutInflater.from(context).inflate(
                    R.layout.cart_extra_addons, null);

            TextView tvAddonName, tvAddonPrice;

            addOnsView.findViewById(R.id.view11).setVisibility(View.GONE);
            tvAddonName = addOnsView.findViewById(R.id.tvAddonName);
            tvAddonPrice = addOnsView.findViewById(R.id.tvAddonPrice);

            tvAddonName.setText(va.getName());
            String adPrice = va.price;

            Double tempAddonPrice = 0.00;
            if (!adPrice.isEmpty() && !adPrice.equalsIgnoreCase("free")) {
                tempAddonPrice += Double.valueOf(Utils.convertToTwoDecimal(adPrice));
            }

            //addonPrice += tempAddonPrice;

            //   tempAddonPrice = quauntityCount * tempAddonPrice;

            tvAddonPrice.setText(tempAddonPrice == 0.0 ? "Free" : "$" + tempAddonPrice);
            linAddons.addView(addOnsView);

        }


    }


    @Override
    public int getItemCount() {
        return cartModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvItemName, TvPrice, TvItemCount;
        public ImageView IvAdd, IvDelete, IvRemove;
        public LinearLayout linAddons;
        public View view11;

        public MyViewHolder(View itemView) {
            super(itemView);
            TvItemName = itemView.findViewById(R.id.TvItemName);
            TvPrice = itemView.findViewById(R.id.TvPrice);
            IvAdd = itemView.findViewById(R.id.IvAddItem);
            IvDelete = itemView.findViewById(R.id.IvDeleteItem);
            TvItemCount = itemView.findViewById(R.id.TvItemCountNo);
            IvRemove = itemView.findViewById(R.id.IvRemove);
            linAddons = itemView.findViewById(R.id.linAddons);
            view11 = itemView.findViewById(R.id.view11);
        }
    }

    private void  removeItemDialogue(int pos) {
        dialog = new Dialog(context, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_delete_item);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llRemove = dialog.findViewById(R.id.llRemove);
        LinearLayout llcancel = dialog.findViewById(R.id.llcancel);
        llcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        llRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartListner.changeSubTotal(pos, 0, 0);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
