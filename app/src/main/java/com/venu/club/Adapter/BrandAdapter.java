package com.venu.club.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Activity.AddToCartItemActivity;
import com.venu.club.Model.VenuBrands;
import com.venu.club.Model.VenuCategory;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;

import java.util.ArrayList;

import static com.venu.club.Utility.MyConstants.KEY_ADDON_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_BRAND_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM_NAME;
import static com.venu.club.Utility.MyConstants.KEY_VENU_CATEGORY_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MENU_ITEM_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;
import static com.venu.club.Utility.Utils.convertToTwoDecimal;

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<VenuBrands> venuBrandsArray;
    public int venuCatId;
    public String menuItemHeaderName;
    public VenuModel selectedVenu;

    public BrandAdapter(Context context, ArrayList<VenuBrands> venuBrandsArray, int venuCatId, VenuModel selectedVenu, String menuItemHeaderName) {
        this.context = context;
        this.venuBrandsArray = venuBrandsArray;
        this.venuCatId = venuCatId;
        this.selectedVenu = selectedVenu;
        this.menuItemHeaderName = menuItemHeaderName;
    }


    @Override
    public BrandAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new BrandAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BrandAdapter.MyViewHolder myViewHolder, int i) {
        VenuBrands vb = venuBrandsArray.get(i);

        if (vb.brandName.equalsIgnoreCase("no brand") || vb.brandName.equalsIgnoreCase("nobrand")) {
            myViewHolder.tvItemName.setText(menuItemHeaderName);
        } else {
            myViewHolder.tvItemName.setText(vb.brandName);
        }

        if (vb.getPrice() != null) {

            myViewHolder.tvPrice.setText(vb.getPrice().startsWith("0.0") ? "Free" : "$" + convertToTwoDecimal(vb.getPrice()));

        } else {
            if (vb.addonsArray.size() > 0) {
                String price = vb.addonsArray.get(0).price;
                myViewHolder.tvPrice.setText(price.startsWith("0.0") ? "Free" : "$" + convertToTwoDecimal(price));
            }
        }

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (vb.addonsArray.size() == 0) {
                    Intent i = new Intent(context, AddToCartItemActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra(KEY_VENU_CATEGORY_ID, vb.venue_item_id);
                    i.putExtra(KEY_VENU_MENU_ITEM_ID, vb.venue_item_id);
                    i.putExtra(KEY_MENU_ITEM_NAME, menuItemHeaderName);
                    i.putExtra(KEY_VENU_MODEL, selectedVenu);
                    i.putExtra(KEY_BRAND_MODEL, vb);
                    //      i.putExtra(KEY_ADDON_MODEL, venuBrandsArray.get(venuBrandPos).addonsArray.get(venuAddonPos));

                    context.getApplicationContext().startActivity(i);
                }
            }
        });


        myViewHolder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (vb.addonsArray.size() == 0) {
                    Intent i = new Intent(context, AddToCartItemActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra(KEY_VENU_MENU_ITEM_ID, vb.venue_item_id);
                    i.putExtra(KEY_VENU_MODEL, selectedVenu);
                    i.putExtra(KEY_MENU_ITEM_NAME, menuItemHeaderName);
                    i.putExtra(KEY_BRAND_MODEL, vb);
                    //      i.putExtra(KEY_ADDON_MODEL, venuBrandsArray.get(venuBrandPos).addonsArray.get(venuAddonPos));

                    context.getApplicationContext().startActivity(i);
                } else {
                    if (myViewHolder.rvItemList.getVisibility() == View.VISIBLE)
                        myViewHolder.rvItemList.setVisibility(View.GONE);
                    else
                        myViewHolder.rvItemList.setVisibility(View.VISIBLE);
                }

            }
        });

        if (vb.addonsArray.size() > 0) {
            setAddonsData(myViewHolder, vb, i);
        }


    }

    private void setAddonsData(BrandAdapter.MyViewHolder myViewHolder, VenuBrands venuBrands, int brandPos) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        myViewHolder.rvItemList.setHasFixedSize(true);
        myViewHolder.rvItemList.setLayoutManager(layoutManager);
        myViewHolder.rvItemList.setNestedScrollingEnabled(false);
        AddonsAdapter addonsAdapter = new AddonsAdapter(context, venuBrands.addonsArray, venuCatId, brandPos);
        myViewHolder.rvItemList.setAdapter(addonsAdapter);
    }

    @Override
    public int getItemCount() {
        return venuBrandsArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName;
        public TextView tvPrice;
        public LinearLayout layoutMain;
        public RecyclerView rvItemList;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            rvItemList = itemView.findViewById(R.id.rvItemList);
            layoutMain = itemView.findViewById(R.id.layoutMain);
        }
    }
}
