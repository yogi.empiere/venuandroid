package com.venu.club.Adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.venu.club.App;
import com.venu.club.Model.SplitFriendModel;
import com.venu.club.R;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AfterSplitFriendAdapter extends RecyclerView.Adapter<AfterSplitFriendAdapter.MyViewHolder> {
    public Context context;
    public List<SplitFriendModel> splitFriendModels;
    public ImageLoader uniImageLoader;
    public static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());


    public AfterSplitFriendAdapter(Context context, List<SplitFriendModel> splitFriendModels) {
        this.context = context;
        this.splitFriendModels = splitFriendModels;
        uniImageLoader = App.initImageLoader(this.context);
    }


    @Override
    public AfterSplitFriendAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_after_split_user, viewGroup, false);
        return new AfterSplitFriendAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AfterSplitFriendAdapter.MyViewHolder myViewHolder, int i) {
        SplitFriendModel splitFriendModel = splitFriendModels.get(i);
        myViewHolder.TvUserName.setText(splitFriendModel.getFirstName());
        myViewHolder.tvPrice.setText("$" + splitFriendModel.getPrice());
        try {

            String profileImage = splitFriendModel.getImageUrl();

            if (profileImage != null && !profileImage.isEmpty()) {


                uniImageLoader.displayImage(profileImage == null ? "" : profileImage, myViewHolder.IvProfile, App.getImageDisplayOptions(),
                        new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                myViewHolder.progressBar.setVisibility(View.GONE);
                                myViewHolder.IvProfile.setImageResource(R.drawable.noimage_rounded);

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                myViewHolder.progressBar.setVisibility(View.GONE);

                                ImageView imageView = (ImageView) view;
                                boolean firstDisplay = !displayedImages.contains(imageUri);
                                if (firstDisplay) {
                                    FadeInBitmapDisplayer.animate(imageView, 500);
                                    displayedImages.add(imageUri);
                                }

                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                myViewHolder.progressBar.setVisibility(View.GONE);
                                myViewHolder.IvProfile.setImageResource(R.drawable.noimage_rounded);

                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current, int total) {

                            }
                        });
            } else {
                myViewHolder.IvProfile.setImageDrawable(context.getDrawable(R.drawable.noimage_rounded));
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return splitFriendModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvUserName, tvPrice;
        public ImageView IvProfile;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            TvUserName = itemView.findViewById(R.id.TvUserName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            IvProfile = itemView.findViewById(R.id.IvProfile);
            progressBar = itemView.findViewById(R.id.progressBar);

        }
    }
}

