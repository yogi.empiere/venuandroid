package com.venu.club.Adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.venu.club.App;
import com.venu.club.Interfaces.SplitFriendListner;
import com.venu.club.Model.SplitFriendModel;
import com.venu.club.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SplitFriendListAdapter extends RecyclerView.Adapter<SplitFriendListAdapter.MyViewHolder> implements Filterable {

    public Context context;
    public List<SplitFriendModel> splitFriendModels;
    public List<SplitFriendModel> filteredsplitFriendModels;
    public SplitFriendListner splitFriendListner;
    public ImageLoader uniImageLoader;
    public static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

    public void setSplitFriendListner(SplitFriendListner splitFriendListner) {
        this.splitFriendListner = splitFriendListner;
        uniImageLoader = App.initImageLoader(this.context);

    }

    public SplitFriendListAdapter(Context context, List<SplitFriendModel> splitFriendModels) {
        this.context = context;
        this.splitFriendModels = splitFriendModels;
        this.filteredsplitFriendModels = splitFriendModels;
        uniImageLoader = App.initImageLoader(this.context);

    }


    @Override
    public SplitFriendListAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_split_users, viewGroup, false);
        return new SplitFriendListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SplitFriendListAdapter.MyViewHolder myViewHolder, int position) {
        SplitFriendModel splitFriendModel = filteredsplitFriendModels.get(position);
        myViewHolder.TvUserName.setText(splitFriendModel.getFirstName());


        try {

            String profileImage = splitFriendModel.getImageUrl();

            if (profileImage != null && !profileImage.isEmpty()) {



                uniImageLoader.displayImage(profileImage == null ? "" : profileImage, myViewHolder.IvProfile, App.getImageDisplayOptions(),
                        new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                myViewHolder.progressBar.setVisibility(View.GONE);
                                myViewHolder.IvProfile.setImageResource(R.drawable.noimage_rounded);

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                myViewHolder.progressBar.setVisibility(View.GONE);

                                ImageView imageView = (ImageView) view;
                                boolean firstDisplay = !displayedImages.contains(imageUri);
                                if (firstDisplay) {
                                    FadeInBitmapDisplayer.animate(imageView, 500);
                                    displayedImages.add(imageUri);
                                }

                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                myViewHolder.progressBar.setVisibility(View.GONE);
                                myViewHolder.IvProfile.setImageResource(R.drawable.noimage_rounded);

                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current, int total) {

                            }
                        });
            } else {
                myViewHolder.IvProfile.setImageDrawable(context.getDrawable(R.drawable.noimage_rounded));
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        myViewHolder.checkbox.setChecked(splitFriendModel.isSelected);


        myViewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myViewHolder.rlList.performClick();
            }
        });

        myViewHolder.rlList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMultiple(position);
            }
        });


    }

    public void checkMultiple(int pos) {

        SplitFriendModel sf = filteredsplitFriendModels.get(pos);

        sf.isSelected = !sf.isSelected;

        if (sf.isSelected) {
            splitFriendListner.addFriend(pos, sf);
        } else {
            splitFriendListner.removeFriend(pos, sf);

        }
        notifyItemChanged(pos);


    }


    @Override
    public int getItemCount() {
        return filteredsplitFriendModels == null ? 0 : filteredsplitFriendModels.size();
        /*return splitFriendModels.size();*/
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvUserName;
        public ImageView IvProfile;
        public CheckBox checkbox;
        public RelativeLayout rlList;
        public ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            TvUserName = itemView.findViewById(R.id.TvUserName);
            IvProfile = itemView.findViewById(R.id.IvProfile);
            checkbox = itemView.findViewById(R.id.checkbox);
            rlList = itemView.findViewById(R.id.rlList);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.d("search", charString);
                if (charString.isEmpty()) {
                    filteredsplitFriendModels = splitFriendModels;
                } else {
                    List<SplitFriendModel> filteredList = new ArrayList<>();

                    for (int i = 0; i < splitFriendModels.size(); i++) {
                        String dataNames = splitFriendModels.get(i).getFirstName();
                        if (dataNames.toLowerCase().contains(charSequence.toString())) {
                            filteredList.add(splitFriendModels.get(i));
                            Log.d("data===", String.valueOf(splitFriendModels.get(i).getFirstName()));
                        }
                    }

                    filteredsplitFriendModels = filteredList;

                }
                if (filteredsplitFriendModels == null) {
                    filteredsplitFriendModels = new ArrayList<SplitFriendModel>();
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredsplitFriendModels;
                filterResults.count = filteredsplitFriendModels.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                filteredsplitFriendModels = (ArrayList<SplitFriendModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


}

