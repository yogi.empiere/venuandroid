package com.venu.club.Adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.venu.club.Model.OpeningHourModel;
import com.venu.club.R;
import java.util.ArrayList;

public class OpeningHourAdapter extends RecyclerView.Adapter<OpeningHourAdapter.MyViewHolder> {
    Context context;
    ArrayList<OpeningHourModel> openingHourModels;

    public OpeningHourAdapter(Context context, ArrayList<OpeningHourModel> openingHourModels) {
        this.context = context;
        this.openingHourModels = openingHourModels;
    }


    @Override
    public OpeningHourAdapter.MyViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.openinghour_list, viewGroup, false);

        return new OpeningHourAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( MyViewHolder myViewHolder, int position) {

        OpeningHourModel openingHourModel = openingHourModels.get(position);
        myViewHolder.tvDay.setText(openingHourModel.getDays());
        myViewHolder.tvTime.setText(openingHourModel.getTime());

    }

    @Override
    public int getItemCount() {
        return openingHourModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDay, tvTime;

        public MyViewHolder( View itemView) {
            super(itemView);
            tvDay = itemView.findViewById(R.id.tvDay);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }
}
