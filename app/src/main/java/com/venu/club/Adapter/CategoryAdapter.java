package com.venu.club.Adapter;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Model.MenuItemListModel;
import com.venu.club.Model.MenuListModel;
import com.venu.club.Model.VenuCategory;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private final VenuModel selectedVenu;
    Context context;
    ArrayList<VenuCategory> venuCatArray;

    public CategoryAdapter(Context context, ArrayList<VenuCategory> menuListModels, VenuModel selectedVenu) {
        this.context = context;
        this.venuCatArray = menuListModels;
        this.selectedVenu = selectedVenu;
     /*   if (this.venuCatArray.size() > 0)
            this.venuCatArray.get(0).isOpened = true;*/
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_list, viewGroup, false);
        return new CategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {
        VenuCategory venuCategory = venuCatArray.get(position);
        myViewHolder.tvItemName.setText(venuCategory.getCategory_name());
        myViewHolder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (venuCategory.isOpened) {
                    venuCategory.isOpened = false;
                    notifyDataSetChanged();
                } else {

                    for (VenuCategory vc : venuCatArray) {
                        vc.isOpened = false;
                    }

                    venuCategory.isOpened = true;
                    notifyDataSetChanged();

                }

            }
        });

        setMenuItemsData(myViewHolder, venuCategory);

        myViewHolder.rvItemList.setVisibility(venuCategory.isOpened ? View.VISIBLE : View.GONE);

    }


    private void setMenuItemsData(MyViewHolder myViewHolder, VenuCategory vCategory) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        myViewHolder.rvItemList.setHasFixedSize(true);
        myViewHolder.rvItemList.setLayoutManager(layoutManager);
        myViewHolder.rvItemList.setNestedScrollingEnabled(false);
        MenuItemAdapter menuItemAdapter = new MenuItemAdapter(context, vCategory.menuItemsArray,  selectedVenu);
        myViewHolder.rvItemList.setAdapter(menuItemAdapter);


    }

    @Override
    public int getItemCount() {
        return venuCatArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName;
        public RecyclerView rvItemList;
        public ImageView ivDown;
        LinearLayout layoutMain;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            rvItemList = itemView.findViewById(R.id.rvItemList);
            ivDown = itemView.findViewById(R.id.IvDown);
            layoutMain = itemView.findViewById(R.id.layoutMain);
        }
    }
}
