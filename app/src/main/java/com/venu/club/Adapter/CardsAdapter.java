package com.venu.club.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.venu.club.Interfaces.BrandAddonsListner;
import com.venu.club.Interfaces.CardListner;
import com.venu.club.Model.CardModel;
import com.venu.club.Model.VenuAddons;
import com.venu.club.R;

import java.util.ArrayList;

import static com.venu.club.Utility.Utils.convertToTwoDecimal;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<CardModel> cardsArray;
    public CardListner cardListner;



    public CardsAdapter(Context context, ArrayList<CardModel> cards) {
        this.context = context;
        this.cardsArray = cards;
        this.cardListner = (CardListner) context;

    }


    @Override
    public CardsAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_card_row, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardsAdapter.MyViewHolder myViewHolder, int position) {
        CardModel cardModel = cardsArray.get(position);
        //    myViewHolder.tvItemName.setText(venuAddons.getName());


        myViewHolder.tvCardType.setText(cardModel.getCardType());
        myViewHolder.tvLast4.setText(" " + cardModel.getCardLast4digit());
        myViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardListner.onDeleteCard(position);
            }
        });


        myViewHolder.relCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardListner.onCardSelect(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return cardsArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLast4, tvCardType;

        public RelativeLayout relCard;
        public ImageView ivDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvLast4 = itemView.findViewById(R.id.tvLast4);
            tvCardType = itemView.findViewById(R.id.tvCardType);
            ivDelete = itemView.findViewById(R.id.ivDelete);

            relCard = itemView.findViewById(R.id.relCard);
        }
    }
}
