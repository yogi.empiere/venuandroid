package com.venu.club.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.venu.club.Activity.VenuDetailActivity;
import com.venu.club.Controls.TextBold;
import com.venu.club.Controls.TextLight;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;
import com.venu.club.Utility.Network.Services;

import java.util.ArrayList;


import io.alterac.blurkit.BlurLayout;

import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    Context context;
    ArrayList<VenuModel> venuModels;

    public HomeAdapter(Context context, ArrayList<VenuModel> venuModels) {
        this.context = context;
        this.venuModels = venuModels;
    }


    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_home_item, viewGroup, false);
        return new HomeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeAdapter.MyViewHolder myViewHolder, int position) {
        VenuModel venuModel = this.venuModels.get(position);

        myViewHolder.tvAddress.setText(venuModel.getAddress());
        myViewHolder.tvDistance.setText(String.format("%.0f", venuModel.getDistance()) + " KM");
        myViewHolder.tvName.setText(venuModel.getName());


        String ImageURL = "";
        if (venuModel.venuImagesArray.size() > 0) {
            ImageURL = Services.BASE_IMAGE_URL + venuModel.venuImagesArray.get(0).image_url;
        }


        myViewHolder.blurLayout.startBlur();

        Glide
                .with(context)
                .load(ImageURL)
                .centerCrop()
                // .placeholder(R.drawable.loading_spinner)
                .into(myViewHolder.IvBar);

        myViewHolder.llBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, VenuDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(KEY_VENU_MODEL, venuModel);
                context.getApplicationContext().startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return venuModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView IvBar;
        public LinearLayout llBar;
        private BlurLayout blurLayout;
        public TextLight tvDistance, tvAddress;
        public TextBold tvName;

        public MyViewHolder(View itemView) {
            super(itemView);
            IvBar = itemView.findViewById(R.id.IvBar);
            llBar = itemView.findViewById(R.id.llBar);
            blurLayout = itemView.findViewById(R.id.blurLayout);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvName = itemView.findViewById(R.id.tvName);
        }
    }


}
