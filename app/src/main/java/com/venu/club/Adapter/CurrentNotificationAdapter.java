package com.venu.club.Adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.venu.club.Model.CurrentNotificationModel;
import com.venu.club.R;

import java.util.ArrayList;

public class CurrentNotificationAdapter extends RecyclerView.Adapter<CurrentNotificationAdapter.MyViewHolder> {

    Context context;
    ArrayList<CurrentNotificationModel> currentNotificationModels;

    public CurrentNotificationAdapter(Context context, ArrayList<CurrentNotificationModel> currentNotificationModels) {
        this.context = context;
        this.currentNotificationModels = currentNotificationModels;
    }


    @Override
    public CurrentNotificationAdapter.MyViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_current_notification, viewGroup, false);
        return new CurrentNotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( CurrentNotificationAdapter.MyViewHolder myViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return currentNotificationModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvNotification, TvTime;

        public MyViewHolder( View itemView) {
            super(itemView);
            TvNotification = itemView.findViewById(R.id.TvNotification);
            TvTime = itemView.findViewById(R.id.TvTime);
        }
    }
}
