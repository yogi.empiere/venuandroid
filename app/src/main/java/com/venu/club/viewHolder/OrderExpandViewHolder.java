package com.venu.club.viewHolder;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.venu.club.R;

public class OrderExpandViewHolder extends ChildViewHolder {

    public TextView TvBarid, TvBarTitle, TvStatus, tvPayNow, tvSplitAmount;
    public RelativeLayout relPayNow;

    public OrderExpandViewHolder(View itemView) {
        super(itemView);
        TvBarid = (TextView) itemView.findViewById(R.id.TvBarid);
        TvBarTitle = (TextView) itemView.findViewById(R.id.TvBarTitle);
        TvStatus = (TextView) itemView.findViewById(R.id.TvStatus);
        tvPayNow = (TextView) itemView.findViewById(R.id.tvPayNow);
        tvSplitAmount = (TextView) itemView.findViewById(R.id.tvSplitAmount);
        relPayNow = itemView.findViewById(R.id.relPayNow);
    }
}
