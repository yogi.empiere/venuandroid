package com.venu.club;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;

import androidx.appcompat.app.ActionBar;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.venu.club.Model.UserModel;
import com.venu.club.Utility.MySharedPref;

import org.json.JSONArray;
import org.json.JSONObject;

public class App extends Application {

    public static UserModel loggedInUser;

    public static App instance;
    public static JSONObject lastVenuMenuData;
    public static JSONArray cartData;
    public static int cartCounter = 0;
    public static int lastVenuID = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        instance = this;
        initImageLoader(getApplicationContext());

    }

    public static ImageLoader initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(150 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader imgLoader = ImageLoader.getInstance();
        imgLoader.init(config.build());
        return imgLoader;
    }


    public static DisplayImageOptions getImageDisplayOptions() {

        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.noimage_rounded)
                .showImageForEmptyUri(R.drawable.noimage_rounded)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(90))
                .build();

    }


}
