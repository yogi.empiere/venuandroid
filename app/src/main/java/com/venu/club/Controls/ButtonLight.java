package com.venu.club.Controls;


import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

public class ButtonLight extends AppCompatButton {

    public ButtonLight(Context context) {
        super(context);
        init();
    }

    public ButtonLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Gotham-Light.otf");
        setTypeface(tf);
    }
}
