package com.venu.club.Controls;


import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

public class ButtonMedium extends AppCompatButton {

    public ButtonMedium(Context context) {
        super(context);
        init();
    }

    public ButtonMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Gotham-Medium.otf");
        setTypeface(tf);
    }
}
