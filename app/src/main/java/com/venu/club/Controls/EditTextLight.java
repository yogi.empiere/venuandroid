package com.venu.club.Controls;


import android.content.Context;
import android.graphics.Typeface;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextLight extends AppCompatEditText {

    public EditTextLight(Context context) {
        super(context);
        init();
    }

    public EditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Gotham-Light.otf");
        setTypeface(tf);
    }
}
