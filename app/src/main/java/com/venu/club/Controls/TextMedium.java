package com.venu.club.Controls;


import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextMedium extends AppCompatTextView {

    public TextMedium(Context context) {
        super(context);
        init();
    }

    public TextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Gotham-Book.otf");
        setTypeface(tf);
    }
}
