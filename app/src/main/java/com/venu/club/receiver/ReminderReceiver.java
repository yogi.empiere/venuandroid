package com.venu.club.receiver;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.venu.club.App;
import com.venu.club.Model.CardModel;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ReminderReceiver extends BroadcastReceiver {


    public MySharedPref mShared;

    @Override
    public void onReceive(Context context, Intent intent) {
        // start service

        System.out.println("------ Alarm Call");

        updateMyLocation(context);
        Utils.scheduleAlarm(context);
    }


    @SuppressLint("LongLogTag")
    public void updateMyLocation(Context context) {
/*
        mySharedPref.setString("lat", String.valueOf(latitude));
        //mySharedPref.setString("lat", "45.5096231");
        mySharedPref.setString("lng", String.valueOf(longitude));*/

        mShared = new MySharedPref(context.getApplicationContext());

        if (mShared.isLogin()) {
            String userId = mShared.getUserId().toString();
            String userName = mShared.getString(mShared.USER_F_NAME, "") + " " + mShared.getString(mShared.USER_L_NAME, "");
            String latitude = mShared.getString(mShared.USER_LATITUDE, "");
            String longitude = mShared.getString(mShared.USER_LONGITUDE, "");


            long currentTime = System.currentTimeMillis();
            Log.i("Response isLogin Reminder Service", "");
            if (!userId.isEmpty() && !latitude.isEmpty() && !longitude.isEmpty()) {


                if (readyForUpdate(currentTime)) {
                    mShared.setString(mShared.LAST_UPDATE_TIME, currentTime + "");
                    Log.i("Response isLogin Reminder latitude", "");
                    HashMap<String, String> postData = new HashMap<>();
                    postData.put("name", userName);
                    postData.put("userId", userId);
                    postData.put("latitude", latitude);
                    postData.put("longitude", longitude);

                    /// remove before goes to live
                //    postData.put("latitude", "-35.2792246");
                //    postData.put("longitude", "149.129526");


                    postData.put("deviceToken", Utils.getDeviceToken());
                    postData.put("deviceType", "android");


                    String URL = Utils.convertToGetURL(Services.VENUES_NEAR_ME, postData);

                    new NetworkCall(context, URL, false, response -> {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("success");

                            if (status) {
                                Log.i("Response Location update :::", jsonObject.toString());
                            }


                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    });
                }
            }
        }


    }

    public boolean readyForUpdate(long currentTime) {


        boolean isContinue = false;
        String strlastAlarmTime = mShared.getString(mShared.LAST_UPDATE_TIME, "0");
        long lastAlarmTime = Long.valueOf(strlastAlarmTime);
        long diffInMillisec = currentTime - lastAlarmTime;
        long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);

        if (lastAlarmTime == 0) {
            isContinue = true;
        } else if (diffInSeconds > 120) {
            isContinue = true;
        }
        return isContinue;
    }
}
