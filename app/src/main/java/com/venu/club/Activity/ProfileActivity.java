package com.venu.club.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.venu.club.Controls.ButtonBook;
import com.venu.club.R;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.venu.club.Utility.MyConstants.COMMON_ERROR_MESSAGE;
import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_TERMS;
import static com.venu.club.Utility.MyConstants.VENU_IMAGE_DIRECTORY;


public class ProfileActivity extends AppCompatActivity {
    private Dialog dialog;
    public Uri selectedUri;
    public String filePath;
    public TextView tvSave, tvCancel, tvEdit, tvChangePic;
    public ButtonBook btnNext;
    public EditText etText;
    public Button btnCancel, btnTakePhoto, btnLibrary;
    public RelativeLayout rl2;
    public LinearLayout llBack;
    public ImageView ivProfile, ivBack;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;
    public MySharedPref mShared;
    public ProfileActivity instance;
    private File file;
    public boolean isFromTerms = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mShared = new MySharedPref(getApplicationContext());
        instance = this;

        if (getIntent() != null) {
            if (getIntent().hasExtra(KEY_IS_FROM_TERMS)) {
                isFromTerms = getIntent().getBooleanExtra(KEY_IS_FROM_TERMS, false);
            }
        }

        bindWidgetReference();
        bindWidgetEvents();
        setData();
    }


    private void bindWidgetEvents() {

       /* rl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });*/
        tvChangePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBack.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.GONE);
                tvSave.setVisibility(View.GONE);
                tvEdit.setVisibility(View.GONE);
                tvChangePic.setVisibility(View.GONE);

                etText.setEnabled(false);

                validAll();
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etText.setEnabled(true);

                ivBack.setVisibility(View.GONE);
                tvCancel.setVisibility(View.VISIBLE);
                tvSave.setVisibility(View.VISIBLE);
                tvEdit.setVisibility(View.GONE);
                tvChangePic.setVisibility(View.VISIBLE);
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBack.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.GONE);
                tvSave.setVisibility(View.GONE);
                tvEdit.setVisibility(View.VISIBLE);
                tvChangePic.setVisibility(View.GONE);

                etText.setEnabled(false);

                validAll();
            setData();

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBack.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.GONE);
                tvSave.setVisibility(View.GONE);
                tvEdit.setVisibility(View.VISIBLE);
                tvChangePic.setVisibility(View.GONE);

                etText.setEnabled(false);
            }
        });

        if (isFromTerms) {
            tvEdit.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
            etText.setEnabled(true);

            ivBack.setVisibility(View.GONE);
            tvCancel.setVisibility(View.VISIBLE);
            tvChangePic.setVisibility(View.VISIBLE);
        } else {
            tvEdit.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);
        }

    }

    private void bindWidgetReference() {
        tvSave = findViewById(R.id.tvSave);
        btnNext = findViewById(R.id.btnNext);
        tvCancel = findViewById(R.id.tvCancel);
        rl2 = findViewById(R.id.rl2);
        ivBack = findViewById(R.id.ivBack);
        tvEdit = findViewById(R.id.tvEdit);
        ivProfile = findViewById(R.id.ivProfile);
        tvChangePic = findViewById(R.id.tvChangePic);
        llBack = findViewById(R.id.llBack);


        etText = findViewById(R.id.etText);
        etText.setEnabled(false);
        tvChangePic.setVisibility(View.GONE);
    }


    private void setData() {

        etText.setText(mShared.getString(mShared.USER_F_NAME, "") + " " + mShared.getString(mShared.USER_L_NAME, ""));

        try {

            String profileImage = mShared.getProfileImageURL();

            if (profileImage != null && !profileImage.isEmpty()) {
                if (!isFromTerms)
                    profileImage = Services.BASE_IMAGE_URL + profileImage;
           /*     Picasso.get()
                        .load(profileImage == null ? "" : profileImage)
                        .fit().centerCrop()
                        .error(R.drawable.noimage)
                        .into(ivProfile);*/


                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                final File directory = cw.getDir(VENU_IMAGE_DIRECTORY, Context.MODE_PRIVATE);

                Picasso.get().load(profileImage == null ? "" : profileImage).
                        into(new Target() {
                            @Override
                            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        file = new File(directory, "profile.jpeg"); // Create image file
                                        FileOutputStream fos = null;
                                        try {
                                            fos = new FileOutputStream(file);
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                            instance.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    ivProfile.setImageBitmap(bitmap);
                                                    ivProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                                }
                                            });
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } finally {
                                            try {
                                                fos.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        Log.i("image", "image saved to >>>" + file.getAbsolutePath());

                                    }
                                }).start();
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                            }


                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Log.i("image", "Device Token>>>" + Utils.getDeviceToken());

    }

    private void createDialog() {

        final BottomSheetDialog dialog = new BottomSheetDialog(ProfileActivity.this, R.style.NewDialog);
        dialog.setContentView(R.layout.dialogue_save);

        btnCancel = dialog.findViewById(R.id.btnCancel);
        btnTakePhoto = dialog.findViewById(R.id.btnTakePhoto);
        btnLibrary = dialog.findViewById(R.id.btnLibrary);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    dialog.dismiss();
                }
               /* CropImage.startPickImageActivity(ProfileActivity.this);
                dialog.dismiss();*/
            }
        });
        btnLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(ProfileActivity.this);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {

                selectedUri = imageUri;
                filePath = selectedUri.getPath();
                file = new File(filePath);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePath = selectedUri.getPath();
                file = new File(filePath);

                ivProfile.setImageBitmap(BitmapFactory.decodeFile(filePath));
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ivProfile.setImageBitmap(photo);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(ProfileActivity.this);
    }

    private void validAll() {
        String name = etText.getText().toString().trim();


        if (name.equals("")) Utils.showWarn(instance, "Please enter name");
            //  else if (file == null) Utils.showWarn(instance, "Please change your profile picture");
        else {

            HashMap<String, String> postData = new HashMap<>();
            postData.put("name", name);
            postData.put("userId", mShared.getUserId() + "");


            HashMap<String, File> postFiles = new HashMap<>();
            if (file != null)
                postFiles.put("profileImage", file);

            new NetworkCall(instance, Services.UPDATE_PROFILE,
                    postData, postFiles, true, response -> {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean success = jsonObject.getBoolean("success");

                    if (success) {

                        JSONObject data = jsonObject.getJSONObject("data");

                        String firstName = data.getString("firstName");
                        String lastName = data.getString("lastName");
                        String ProfileImage = data.getString("imageUrl");
                        boolean firstLogin = data.getBoolean("firstLogin");


                        mShared.setString(mShared.USER_F_NAME, firstName);
                        mShared.setString(mShared.USER_L_NAME, lastName);
                        mShared.setBoolean(mShared.USER_IS_FIRST_TIME, firstLogin);
                        mShared.setString(mShared.USER_PROFILE_IMAGE, ProfileImage);

                        if (isFromTerms) {
                            Intent i = new Intent(instance,
                                    HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        } else {

                            Utils.showWarn(instance, "Profile Updated Successfully", 2, new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    onBackPressed();
                                }
                            });

                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }


}
