package com.venu.club.Activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.JsonObject;
import com.venu.club.Adapter.CategoryAdapter;
import com.venu.club.App;
import com.venu.club.Model.VenuAddons;
import com.venu.club.Model.VenuBrands;
import com.venu.club.Model.VenuCategory;
import com.venu.club.Model.VenuMenuItems;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Utility.MyConstants.DEFAULT_PAGE_RECORDS;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;
import static com.venu.club.Utility.MyConstants.OWNER_GROUP_ID;

public class MenuActivity extends AppCompatActivity {

    RecyclerView rvMenuList;
    public RecyclerView.LayoutManager layoutManager;
    public CategoryAdapter menuAdapter;
    public ArrayList<VenuCategory> venuCatArray = new ArrayList<>();
    public ImageView ivBack;
    public ImageBadgeView Ivcart;
    public LinearLayout llBack;
    public JsonParserUniversal jParser;
    public MenuActivity instance;
    public MySharedPref myShared;
    public VenuModel selectedVenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        instance = this;
        jParser = new JsonParserUniversal();
        myShared = new MySharedPref(getApplicationContext());

        selectedVenu = (VenuModel) getIntent().getSerializableExtra(KEY_VENU_MODEL);


        bindWidgetReference();
        bindWidgetEvents();

     /*   if (App.lastVenuID == selectedVenu.venueId && App.lastVenuMenuData != null) {
            parseMenu(App.lastVenuMenuData);
        } else {
            App.lastVenuID = selectedVenu.venueId;
            getAllMenuItems(0);

        }*/

        getAllMenuItems(0);


    }


    private void bindWidgetEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                /*Intent i = new Intent(MenuActivity.this, VenuDetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(KEY_VENU_MODEL, selectedVenu);
                startActivity(i);*/
            }
        });
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(instance,
                        HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this, CartActivity.class);
                startActivity(i);
            }
        });
    }

    private void bindWidgetReference() {
        ivBack = findViewById(R.id.ivBack);
        rvMenuList = findViewById(R.id.rvMenuList);
        Ivcart = findViewById(R.id.Ivcart);
        llBack = findViewById(R.id.llBack);

        Ivcart.setVisibility(myShared.isLogin() ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.setCartBadge(Ivcart);
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rvMenuList.setHasFixedSize(true);
        rvMenuList.setLayoutManager(layoutManager);
        menuAdapter = new CategoryAdapter(getApplicationContext(), venuCatArray, selectedVenu);
        rvMenuList.setAdapter(menuAdapter);
    }

    private void getAllMenuItems(int page) {
/*page:1
size:10
userId:3
venueId:3
groupId:*/

        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("size", DEFAULT_PAGE_RECORDS);
        postData.put("groupId", OWNER_GROUP_ID);
        postData.put("userId", myShared.getUserId() + "");
        postData.put("venueId", selectedVenu.getVenueId() + "");


        String URL = Utils.convertToGetURL(Services.GET_MENU_DETAIL, postData);
       // String URL = Utils.convertToGetURL("http://live-api.venuapp.club/api/v1/customer/getMenuDetail", postData);

        new NetworkCall(instance, URL, true, response -> {
            try {


                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    JSONObject jData = jsonObject.getJSONObject("data");

                    App.lastVenuMenuData = jData;
                    parseMenu(jData);

                }


            } catch (Exception e) {
                e.printStackTrace();
              //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });


    }

    public void parseMenu(JSONObject jData) {
        try {
            int noOFResults = jData.getInt("numberOfResults");

            if (noOFResults > 0) {
                venuCatArray.clear();
                JSONArray jContentArray = jData.getJSONArray("contentList");

                JSONArray jUserMenuArray = jContentArray.getJSONObject(0).getJSONArray("userMenuModelList");
                for (int c = 0; c < jUserMenuArray.length(); c++) {
                    VenuCategory venuCat = (VenuCategory) jParser.parseJson(jUserMenuArray.getJSONObject(c), new VenuCategory());
                    if (jUserMenuArray.getJSONObject(c).has("display-order")) {
                        venuCat.display_order = jUserMenuArray.getJSONObject(c).getInt("display-order");
                    }

                    JSONArray jMenuItems = jUserMenuArray.getJSONObject(c).getJSONArray("menuItems");
                    if (jMenuItems.length() > 0) {
                        for (int mi = 0; mi < jMenuItems.length(); mi++) {
                            VenuMenuItems vMenuItems = new VenuMenuItems();
                            vMenuItems.name = jMenuItems.getJSONObject(mi).getString("name");

                            JSONArray jBrands = jMenuItems.getJSONObject(mi).getJSONArray("brands");
                            if (jBrands.length() > 0) {
                                for (int br = 0; br < jBrands.length(); br++) {
                                    VenuBrands vBrands = (VenuBrands) jParser.parseJson(jBrands.getJSONObject(br), new VenuBrands());

                                    JSONArray jAddons = jBrands.getJSONObject(br).getJSONArray("addOns");
                                    if (jAddons.length() > 0) {
                                        for (int ad = 0; ad < jAddons.length(); ad++) {
                                            VenuAddons vAddons = (VenuAddons) jParser.parseJson(jAddons.getJSONObject(ad), new VenuAddons());
                                            vBrands.addonsArray.add(vAddons);
                                        }

                                    }

                                    vMenuItems.brandsArray.add(vBrands);
                                }
                            }


                            venuCat.menuItemsArray.add(vMenuItems);
                        }
                    }

                    venuCatArray.add(venuCat);

                }

                setData();


            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
