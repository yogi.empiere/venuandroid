package com.venu.club.Activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.venu.club.Adapter.BrandAdapter;
import com.venu.club.Controls.TextBold;
import com.venu.club.Controls.TextLight;
import com.venu.club.Interfaces.BrandAddonsListner;
import com.venu.club.Model.VenuAddons;
import com.venu.club.Model.VenuBrands;
import com.venu.club.Model.VenuMenuItems;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Utils;

import java.util.ArrayList;

import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Utility.MyConstants.KEY_ADDON_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_BRAND_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM_NAME;
import static com.venu.club.Utility.MyConstants.KEY_VENU_CATEGORY_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MENU_ITEM_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;

public class BrandAddonsActivity extends AppCompatActivity implements BrandAddonsListner {
    RecyclerView rvItemList;
    public RecyclerView.LayoutManager layoutManager;
    public BrandAdapter brandAdapter;
    public ArrayList<VenuBrands> venuBrandsArray;
    public ImageView ivBack;
    public ImageBadgeView Ivcart;
    public Button btnNext;
    public LinearLayout llBack;
    public VenuMenuItems vMenuItem;
    public int venuMenuItemID = -1, venuBrandPos = 0, venuAddonPos = 0;
    public TextLight tvMenuItemName;
    public TextBold tvAmount;
    public Double finalAmount = 0.00;
    public BrandAddonsActivity instance;
    public VenuModel selectedVenu;
    public ArrayList<VenuAddons> selectedAddonsArray = new ArrayList<>();

    public static BrandAddonsActivity mBrandAddonsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_addons);
        mBrandAddonsActivity = this;

        instance = this;

        vMenuItem = (VenuMenuItems) getIntent().getSerializableExtra(KEY_MENU_ITEM);
        selectedVenu = (VenuModel) getIntent().getSerializableExtra(KEY_VENU_MODEL);

        venuBrandsArray = vMenuItem.brandsArray;

        bindWidgetReference();
        bindWidgetEvents();
        setData();
    }

    private void bindWidgetEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (venuMenuItemID == -1) {
                    Utils.showToast(instance, "Please select any brand.");
                    return;
                }
                btnNext.setTextColor(getResources().getColor(R.color.textcolor));
                Intent i = new Intent(BrandAddonsActivity.this, AddToCartItemActivity.class);
                i.putExtra(KEY_VENU_MENU_ITEM_ID, venuMenuItemID);
                i.putExtra(KEY_BRAND_MODEL, venuBrandsArray.get(venuBrandPos));
                i.putExtra(KEY_MENU_ITEM_NAME, vMenuItem.getName());
                i.putExtra(KEY_ADDON_MODEL, selectedAddonsArray);
                i.putExtra(KEY_VENU_MODEL, selectedVenu);

                startActivity(i);
            }
        });

        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BrandAddonsActivity.this, CartActivity.class);
                startActivity(i);
            }
        });
    }

    private void bindWidgetReference() {

        rvItemList = findViewById(R.id.rvItemList);
        ivBack = findViewById(R.id.ivBack);
        Ivcart = findViewById(R.id.Ivcart);
        btnNext = findViewById(R.id.btnNext);
        llBack = findViewById(R.id.llBack);
        tvMenuItemName = findViewById(R.id.tvMenuItemName);
        tvAmount = findViewById(R.id.tvAmount);


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (new MySharedPref(getApplicationContext()).isLogin()) {
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }

    }


    private void setData() {
        tvMenuItemName.setText(vMenuItem.getName());

        layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rvItemList.setHasFixedSize(true);
        rvItemList.setLayoutManager(layoutManager);
        brandAdapter = new BrandAdapter(instance, venuBrandsArray, venuMenuItemID, selectedVenu, vMenuItem.getName());
        rvItemList.setAdapter(brandAdapter);
    }

    @Override
    public void onAddonClick(int brandPos, int addonPos) {

        try {

            String brandPrice = venuBrandsArray.get(brandPos).price;
            venuMenuItemID = venuBrandsArray.get(brandPos).venue_item_id;

            VenuAddons selectedAddon = venuBrandsArray.get(brandPos).addonsArray.get(addonPos);

            boolean isExist = false;
            for (VenuAddons va : new ArrayList<VenuAddons>(selectedAddonsArray)) {
                if (va.id == selectedAddon.id) {
                    selectedAddonsArray.remove(va);
                    isExist = true;
                }
            }

            if (!isExist)
                selectedAddonsArray.add(selectedAddon);


            venuAddonPos = addonPos;
            venuBrandPos = brandPos;

            Double dBPrice = 0.00, dAPrice = 0.00;

            if (!brandPrice.isEmpty() && !brandPrice.equalsIgnoreCase("free")) {
                dBPrice = Double.valueOf(Utils.convertToTwoDecimal(brandPrice));
            }


            for (VenuAddons va : selectedAddonsArray) {
                String addOnsPrice = va.price;
                if (!addOnsPrice.isEmpty() && !addOnsPrice.equalsIgnoreCase("free")) {
                    dAPrice += Double.valueOf(Utils.convertToTwoDecimal(addOnsPrice));
                }
            }


            finalAmount = dBPrice + dAPrice;

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (java.util.ConcurrentModificationException e) {
            e.printStackTrace();
        }
        tvAmount.setText("$" + Utils.convertToTwoDecimal(finalAmount + ""));

    }
}
