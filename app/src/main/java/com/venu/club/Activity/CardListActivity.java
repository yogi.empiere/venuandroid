package com.venu.club.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.venu.club.Adapter.CardsAdapter;
import com.venu.club.App;
import com.venu.club.Controls.TextLight;
import com.venu.club.Interfaces.CardListner;
import com.venu.club.Model.CardModel;
import com.venu.club.R;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.venu.club.Utility.MyConstants.KEY_CARD_ID;
import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;
import static com.venu.club.Utility.MyConstants.KEY_INTENT_TRANSFER;
import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_SETTINGS;
import static com.venu.club.Utility.MyConstants.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.venu.club.Utility.MySharedPref.USER_LATITUDE;
import static com.venu.club.Utility.MySharedPref.USER_LONGITUDE;
import static com.venu.club.Utility.Network.Services.CHARGE_CUSTOMER;
import static com.venu.club.Utility.Network.Services.CHARGE_CUSTOMER_CART;
import static com.venu.club.Utility.Network.Services.CHARGE_SPLIT_CUSTOMER;
import static com.venu.club.Utility.Utils.checkGPS;

public class CardListActivity extends AppCompatActivity implements CardListner, GoogleApiClient.ConnectionCallbacks,
        LocationListener {
    public ImageView ivBack, Ivcart;
    public TextLight tvAddCard;
    public CardListActivity instance;
    public ArrayList<CardModel> cardsArray;

    public boolean isFromCart = false;
    public RecyclerView rvCards;
    public LinearLayoutManager layoutManager;
    public CardsAdapter cardAdapter;
    public MySharedPref myShared;
    public HashMap<String, String> orderMap;
    public boolean isCartPayment = false, isSplitPayment = false, isFromSettings = false;
    private GoogleApiClient mGoogleApiClient;
    public Intent recvIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_setting);
        instance = this;
        myShared = new MySharedPref(getApplicationContext());


        if (getIntent() != null) {
            recvIntent = getIntent();
            if (recvIntent.hasExtra(KEY_IS_FROM_SETTINGS)) {
                isFromSettings = recvIntent.getBooleanExtra(KEY_IS_FROM_SETTINGS, false);

            } else if (recvIntent.hasExtra(KEY_CART_PAYMENT)) {
                isCartPayment = recvIntent.getBooleanExtra(KEY_CART_PAYMENT, false);
                orderMap = (HashMap<String, String>) recvIntent.getSerializableExtra(KEY_CART_MAP);
                isSplitPayment = orderMap.get("isSplit").equalsIgnoreCase("Yes") ? true : false;

                for (Map.Entry<String, String> entry : orderMap.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();

                    System.out.println("key-- " + key + " -- " + value);
                    // do stuff
                }


            }


        } else {
            recvIntent = null;
        }


        bindWidgetReference();
        bindWidgetEvents();
        getUserCards(0);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void bindWidgetEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //   Intent i = new Intent(CardListActivity.this, );
                //  startActivity(i);
            }
        });
        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(instance, CartActivity.class);
                startActivity(i);
            }
        });

        tvAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(instance, AddCardActivity.class);

                if (recvIntent != null)
                    i.putExtra(Intent.EXTRA_INTENT, (Parcelable) recvIntent);
                startActivityForResult(i, 0);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            recvIntent = (Intent) data;

            String cardID = data.getStringExtra(KEY_CARD_ID);


            if (recvIntent.hasExtra(KEY_IS_FROM_SETTINGS)) {
                System.out.println("Venu-->" + "IsFromSettings");
                isFromSettings = recvIntent.getBooleanExtra(KEY_IS_FROM_SETTINGS, false);

            } else if (recvIntent.hasExtra(KEY_CART_PAYMENT)) {
                isCartPayment = recvIntent.getBooleanExtra(KEY_CART_PAYMENT, false);
                orderMap = (HashMap<String, String>) recvIntent.getSerializableExtra(KEY_CART_MAP);

                isSplitPayment = orderMap.get("isSplit").equalsIgnoreCase("Yes") ? true : false;
            }

            if (orderMap != null) {

                orderMap.put("cardID", cardID);
                orderMap.put("latitude", myShared.getLatitude());
                orderMap.put("longitude", myShared.getLongitude());
                //    orderMap.put("latitude", "-35.2792246");
                //   orderMap.put("longitude", "149.129526");


                chargeCustomerCart();
            } else {
                getUserCards(0);
            }


        } else {
            getUserCards(0);
        }
    }

    private void bindWidgetReference() {

        ivBack = findViewById(R.id.ivBack);
        Ivcart = findViewById(R.id.Ivcart);
        tvAddCard = findViewById(R.id.tvAddCard);
        rvCards = findViewById(R.id.rvCards);

    }

    private void getUserCards(int page) {

        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("size", MyConstants.DEFAULT_PAGE_RECORDS);
        postData.put("userId", String.valueOf(myShared.getUserId()));

        String URL = Utils.convertToGetURL(Services.GET_USER_CARDS, postData);

        new NetworkCall(instance, URL, true, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    cardsArray = new ArrayList<>();
                    JSONArray jCardsArray = jsonObject.getJSONArray("data");
                    Gson gson = new Gson();
                    cardsArray = gson.fromJson(jCardsArray.toString(), new TypeToken<List<CardModel>>() {
                    }.getType());

                    setData();

                }


            } catch (Exception e) {
                e.printStackTrace();

                //Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });

    }

    private void setData() {
        layoutManager = new LinearLayoutManager(instance, RecyclerView.VERTICAL, false);
        rvCards.setHasFixedSize(true);
        rvCards.setLayoutManager(layoutManager);
        rvCards.setItemViewCacheSize(2);
        cardAdapter = new CardsAdapter(instance, cardsArray);

        rvCards.setAdapter(cardAdapter);

    }

    @Override
    public void onCardSelect(int pos) {

        if (isFromSettings) {
            return;
        }

        orderMap.put("cardID", cardsArray.get(pos).getCardId());

        if (myShared.getLatitude().isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)
                    buildGoogleApiClient();
                else
                    checkLocationPermission();
            } else
                buildGoogleApiClient();

        } else {

            orderMap.put("latitude", myShared.getLatitude());
            orderMap.put("longitude", myShared.getLongitude());

            if (isSplitPayment) {
                if (orderMap.containsKey("orderId")) {
                    try {
                        chargeSplitCustomer(Integer.valueOf(orderMap.get("orderId")));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        Utils.showToast(instance, "Invalid data, Data format expection, Please try again later");
                    }
                } else {
                    chargeCustomerCart();
                }
            } else {
                chargeCustomerCart();
            }

        }


    }

    public void chargeCustomerCart() {
        String paymentURL = CHARGE_CUSTOMER;

        if (isCartPayment) {
            paymentURL = CHARGE_CUSTOMER_CART;
        } else if (isSplitPayment) {
            paymentURL = CHARGE_SPLIT_CUSTOMER;
        }


        orderMap.put("latitude", myShared.getLatitude());
        orderMap.put("longitude", myShared.getLongitude());
        // orderMap.put("latitude", "-35.2792246");
        // orderMap.put("longitude", "149.129526");
        new NetworkCall(instance, paymentURL, orderMap,
                true, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");


                if (status) {
                    if (isSplitPayment) {
                        if (!jsonObject.has("data")) {
                            redirectToPaymentSuccess();
                        } else {
                            int orderID = jsonObject.getJSONObject("data").getInt("order_id");
                            chargeSplitCustomer(orderID);
                        }
                    } else {
                        redirectToPaymentSuccess();
                    }
                } else {
                    try {
                        String message = jsonObject.getString("message");
                        Utils.showWarn(instance, message, 3);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getUserCards(0);
                }

            } catch (Exception e) {
                Utils.showToast(instance, "Please try again later!");
                e.printStackTrace();
                getUserCards(0);
            }
        });


    }

    public void chargeSplitCustomer(int orderId) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", myShared.getUserId().toString());
        postData.put("cardID", orderMap.get("cardID"));
        postData.put("orderId", orderId + "");


        new NetworkCall(instance, CHARGE_SPLIT_CUSTOMER, postData,
                true, response -> {
            try {


                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    redirectToPaymentSuccess();
                } else {
                    try {
                        String message = jsonObject.getString("message");
                        Utils.showWarn(instance, message, 3);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

                //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });
    }

    public void redirectToPaymentSuccess() {
        App.cartCounter = 0;
        Intent i = new Intent(instance, PaymentSuccesfulActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        instance.finish();
    }

    public void checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(instance,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(instance,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(instance,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(instance,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(instance)
                .addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        System.out.println("google api client connected");


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted. Do the
                // contacts-related task you need to do.
                System.out.println("granted before if");
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    return;
                }

            }

            Utils.showWarn(instance, MyConstants.DECLINE_LOCATION_MESSAGE);


            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        System.out.println("on connected");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(instance,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on suspended");
    }

    private double latitude = 0.0, longitude = 0.0;

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        location.setProvider(checkGPS(instance) ? LocationManager.GPS_PROVIDER
                : LocationManager.NETWORK_PROVIDER);

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        MySharedPref mySharedPref = new MySharedPref(getApplicationContext());
        mySharedPref.setString(USER_LATITUDE, String.valueOf(latitude));
        mySharedPref.setString(USER_LONGITUDE, String.valueOf(longitude));

        System.out.println("lat---->" + latitude + " long---->" + longitude);

        orderMap.put("latitude", myShared.getLatitude());
        orderMap.put("longitude", myShared.getLongitude());

        chargeCustomerCart();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }


    @Override
    public void onDeleteCard(int pos) {

        dialog = new Dialog(instance, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_delete_card);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llcancel = dialog.findViewById(R.id.llcancel);
        LinearLayout llRemove = dialog.findViewById(R.id.llRemove);
        llRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCardAPICall(pos);
                dialog.dismiss();
            }
        });
        llcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.show();

    }


    public Dialog dialog;

    public void deleteCardAPICall(int pos) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", myShared.getUserId().toString());
        postData.put("cardId", cardsArray.get(pos).cardId);


        String URL = Utils.convertToGetURL(Services.DELETE_CARD, postData);

        new NetworkCall(instance, URL, true, response -> {
            try {


                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    cardsArray.remove(pos);
                    cardAdapter.notifyDataSetChanged();
                    Utils.showToast(instance, "Card removed successfully");

                }
            } catch (Exception e) {
                e.printStackTrace();
                //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });
    }

}
