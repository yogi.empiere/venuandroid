package com.venu.club.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.venu.club.Controls.TextLight;
import com.venu.club.R;

import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_TERMS;

public class TermsAndConditionActivity extends AppCompatActivity {

    public TextLight tvText;
    public TextLight tvAccept, tvDecline;
    public TermsAndConditionActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        instance = this;

        tvText = findViewById(R.id.tvText);
        tvAccept = findViewById(R.id.tvAccept);
        tvDecline = findViewById(R.id.tvDecline);


        customTextView(tvText);


        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(instance,
                        ProfileActivity.class);
                i.putExtra(KEY_IS_FROM_TERMS, true);
                startActivity(i);
                finish();
            }
        });
        tvDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(instance,
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }


    private void customTextView(TextLight view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "By selecting accept, you agree to the ");
        spanTxt.append("terms and conditions");


        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent i = new Intent(instance,
                        EndUserLicenceActivity.class);
                startActivity(i);
            }
        },
                spanTxt.length() - "terms and conditions".length(), spanTxt.length(), 0);


        spanTxt.append(" and");
        spanTxt.append(" privacy policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent i = new Intent(instance,
                        PrivacyPolicyActivity.class);

                startActivity(i);

            }
        }, spanTxt.length() - " privacy policy".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }
}
