package com.venu.club.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.venu.club.Fragment.OrderHistoryFragment;
import com.venu.club.R;

import static com.venu.club.Utility.MyConstants.KEY_ORDER_HISTORY_REDIRECT;

public class PaymentSuccesfulActivity extends AppCompatActivity {
    public Button btnViewOrderHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_accepted);


        btnViewOrderHistory = findViewById(R.id.btnViewOrderHistory);
        btnViewOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PaymentSuccesfulActivity.this, HomeActivity.class);
                i.putExtra(KEY_ORDER_HISTORY_REDIRECT, true);
                startActivity(i);
                finish();
            }
        });
    }
}
