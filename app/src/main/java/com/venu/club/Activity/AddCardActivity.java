package com.venu.club.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;
import com.venu.club.Controls.TextLight;
import com.venu.club.R;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import static com.venu.club.Utility.MyConstants.KEY_CARD_ID;
import static com.venu.club.Utility.MyConstants.KEY_INTENT_TRANSFER;

public class AddCardActivity extends AppCompatActivity {

    public TextLight tvSave, tvCancel;
    public Card myCard;
    public CardMultilineWidget cardWidget;
    public AddCardActivity instance;
    public MySharedPref mShared;

    public boolean isFromPaymentScreen = false;
    public Intent recvdIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        instance = this;
        mShared = new MySharedPref(getApplicationContext());

        bindWidgetReference();
        bindWidgetEvents();

        if (getIntent() != null) {
            if (getIntent().hasExtra(Intent.EXTRA_INTENT)) {
                isFromPaymentScreen = true;
                recvdIntent = (Intent) getIntent().getParcelableExtra(Intent.EXTRA_INTENT);
            }
        }
    }

    private void bindWidgetEvents() {
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //   Intent i = new Intent(CardListActivity.this, );
                //  startActivity(i);
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndCreateToken(cardWidget.getCard());

            }
        });


    }

    private void bindWidgetReference() {

        tvSave = findViewById(R.id.tvSave);
        tvCancel = findViewById(R.id.tvCancel);
        cardWidget = findViewById(R.id.cardWidget);

    }

    public void validateAndCreateToken(Card card) {

        boolean validation = card.validateCard();
        if (validation) {
            //startProgress("Validating Credit Card");


            Utils.showPopup(instance);


            String key = Utils.getStripePublishableKey(instance);

            new Stripe(instance, key).createToken(card,
                    new ApiResultCallback<Token>() {
                        @Override
                        public void onSuccess(@NonNull Token result) {
                            saveCardToServer(result);
                        }

                        @Override
                        public void onError(@NonNull Exception e) {
                            Utils.closePopup();
                            ;
                            Utils.showToast(instance, "Stripe -" + e.toString());
                        }
                    });


        } else if (!card.validateNumber()) {
            Utils.showToast(instance, "The card number that you entered is invalid");
        } else if (!card.validateExpiryDate()) {
            Utils.showToast(instance, "The expiration date that you entered is invalid");
        } else if (!card.validateCVC()) {
            Utils.showToast(instance, "The CVC code that you entered is invalid");
        } else {
            Utils.showToast(instance, "The card details that you entered are invalid");

        }


    }

    public void saveCardToServer(Token token) {

      /*  static let addCardAPI ="addCard"
                --> params (POST) = "cardToken", "cardId", "cardType" = "VISA", "lastDigitCard" , "addCard" = true/false  , "userId"
*/

        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", mShared.getUserId().toString());
        postData.put("cardToken", token.getId());
        postData.put("cardId", token.getCard().getId());
        postData.put("cardType", token.getCard().getBrand());
        postData.put("addCard", "true");
        postData.put("lastDigitCard", token.getCard().getLast4());

        new NetworkCall(instance, Services.ADD_CARD, postData,
                true, response -> {
            try {

                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    Utils.showToast(instance, "Card added Successfully");

                    if (isFromPaymentScreen) {
                        Intent intent = new Intent();
                        intent = (Intent) recvdIntent.clone();
                        intent.putExtra(KEY_CARD_ID, token.getCard().getId());
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        onBackPressed();
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
        });
    }

}
