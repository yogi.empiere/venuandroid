package com.venu.club.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.venu.club.Adapter.AfterSplitFriendAdapter;
import com.venu.club.Adapter.SplitFriendListAdapter;
import com.venu.club.Controls.ButtonLight;
import com.venu.club.Controls.EditTextLight;
import com.venu.club.Controls.TextLight;
import com.venu.club.Controls.TextMedium;
import com.venu.club.Interfaces.SplitFriendListner;
import com.venu.club.Model.SplitFriendModel;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Activity.HomeActivity.Ivcart;
import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;
import static com.venu.club.Utility.MyConstants.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.venu.club.Utility.MySharedPref.USER_LATITUDE;
import static com.venu.club.Utility.MySharedPref.USER_LONGITUDE;
import static com.venu.club.Utility.Utils.checkGPS;

public class SplitPaymentActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        LocationListener {


    RecyclerView rvSplit, rvSplitFriendlist;
    public RecyclerView.LayoutManager layoutManager;
    public AfterSplitFriendAdapter afterSplitFriendAdapter;
    public SplitFriendListAdapter splitFriendListAdapter;
    public List<SplitFriendModel> splitFriendModels, afterSplitFriendsArray;
    public ImageView ivBack;
    public ImageBadgeView Ivcart;
    public TextMedium TvTotalPrice;
    public TextLight TvCancel;
    public double mainTotal = 0.0, splitAmount;
    public JsonParserUniversal jParser;
    public MySharedPref myShared;
    public EditTextLight EtSearch;
    public ButtonLight btnPayNow;
    public String splitIds = "";
    public HashMap<String, String> orderMap = new HashMap<>();
    public boolean isCartPayment = false;
    public SplitPaymentActivity instance;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_payment);
        instance = this;
        myShared = new MySharedPref(getApplicationContext());
        jParser = new JsonParserUniversal();

        if (getIntent() != null) {
            if (getIntent().hasExtra(KEY_CART_PAYMENT)) {
                isCartPayment = getIntent().getBooleanExtra(KEY_CART_PAYMENT, false);
                orderMap = (HashMap<String, String>) getIntent().getSerializableExtra(KEY_CART_MAP);

                double tempTotal = Double.parseDouble(orderMap.get("totalAmount"));
                mainTotal = tempTotal;
            }
        }


        if (myShared.getLatitude().isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)
                    buildGoogleApiClient();
                else
                    checkLocationPermission();
            } else
                buildGoogleApiClient();

        } else {
            getFriendsData();
        }

        bindWidgetReference();
        bindWidgetEvents();


        setSplitData();


    }

    private void bindWidgetReference() {
        rvSplit = findViewById(R.id.rvSplit);
        rvSplitFriendlist = findViewById(R.id.rvSplitFriendlist);
        ivBack = findViewById(R.id.IvBack);
        Ivcart = findViewById(R.id.Ivcart);
        TvCancel = findViewById(R.id.TvCancel);
        TvTotalPrice = findViewById(R.id.TvTotalPrice);
        EtSearch = findViewById(R.id.EtSearch);
        btnPayNow = findViewById(R.id.btnPayNow);
        TvTotalPrice.setText("$" + Utils.convertToTwoDecimal(mainTotal));
    }

    private void bindWidgetEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        TvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        EtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (splitFriendListAdapter != null) {
                        splitFriendListAdapter.getFilter().filter(s.toString());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payNow();
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        if(myShared.isLogin()){
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }

    }


    public void payNow() {

        if (afterSplitFriendsArray.size() == 1) {
            Utils.showToast(instance, "Please select your friend to split bill.");
            return;
        }

        if (orderMap.size() == 0) {
            Utils.showToast(instance, "Something went wrong, Please try again later!");
        }


        ArrayList<String> tempSplitIds = new ArrayList<>();
        for (int i = 0; i < afterSplitFriendsArray.size(); i++) {
            tempSplitIds.add(afterSplitFriendsArray.get(i).userID + "");
        }

        splitIds = android.text.TextUtils.join(",", tempSplitIds);
        orderMap.put("splitIds", splitIds);

        Double tempMainTotal = Double.valueOf(orderMap.get("totalAmount")+"");

        double splitAmount = tempMainTotal / afterSplitFriendsArray.size();
        orderMap.put("splitAmount", (splitAmount) + "");
        orderMap.put("isSplit", "Yes");
        Intent i = new Intent(instance, CardListActivity.class);
        i.putExtra(KEY_CART_PAYMENT, true);
        i.putExtra(KEY_CART_MAP, orderMap);
        startActivity(i);


    }


    @SuppressLint("WrongConstant")
    private void setSplitData() {

        if (afterSplitFriendsArray == null) {
            afterSplitFriendsArray = new ArrayList<>();
        }

        SplitFriendModel spMe = new SplitFriendModel();
        spMe.setFirstName("Me");
        spMe.setUserID(myShared.getUserId());
        spMe.setImageUrl(myShared.getString(myShared.USER_PROFILE_IMAGE, ""));
        spMe.setPrice(mainTotal + "");
        afterSplitFriendsArray.add(spMe);

        layoutManager = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);
        rvSplit.setHasFixedSize(true);
        rvSplit.setNestedScrollingEnabled(false);
        rvSplit.setLayoutManager(layoutManager);
        rvSplit.setItemViewCacheSize(2);
        afterSplitFriendAdapter = new AfterSplitFriendAdapter(this, afterSplitFriendsArray);

        rvSplit.setAdapter(afterSplitFriendAdapter);


        Utils.hideKeyboard(instance);
    }


    public void checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(instance,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(instance,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(instance,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(instance,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(instance)
                .addConnectionCallbacks(instance)
                //.addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        System.out.println("google api client connected");


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted. Do the
                // contacts-related task you need to do.
                System.out.println("granted before if");
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    return;
                }

            }

            Utils.showWarn(instance, MyConstants.DECLINE_LOCATION_MESSAGE);


            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        System.out.println("on connected");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(instance,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on suspended");
    }

    private double latitude = 0.0, longitude = 0.0;

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");

        location.setProvider(checkGPS(instance) ? LocationManager.GPS_PROVIDER
                : LocationManager.NETWORK_PROVIDER);

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();


        myShared.setString(USER_LATITUDE, String.valueOf(latitude));
        myShared.setString(USER_LONGITUDE, String.valueOf(longitude));

        System.out.println("lat---->" + latitude + " long---->" + longitude);

        orderMap.put("latitude", myShared.getLatitude());
        orderMap.put("longitude", myShared.getLongitude());

        getFriendsData();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }


    private void getFriendsData() {


        HashMap<String, String> postData = new HashMap<>();
        postData.put("latitude", myShared.getLatitude());
        postData.put("longitude", myShared.getLongitude());
        postData.put("userId", myShared.getUserId()+"");

        /// remove this before Send to app store or testing
      //  postData.put("latitude", "-35.2792246");
      //  postData.put("longitude", "149.129526");

        String URL = Utils.convertToGetURL(Services.SPLIT_USER_FRIEND_LIST, postData);

        new NetworkCall(SplitPaymentActivity.this, URL, true, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    splitFriendModels = new ArrayList<>();
                    splitFriendModels.clear();

                    JSONArray jsonData = jsonObject.getJSONArray("data");

                    int userID = myShared.getUserId();
                    for (int i = 0; i < jsonData.length(); i++) {
                        JSONObject jCartObj = jsonData.getJSONObject(i);
                        SplitFriendModel splitFriendModel = (SplitFriendModel) jParser.parseJson(jCartObj, new SplitFriendModel());

                        if (userID != splitFriendModel.userID) {
                            splitFriendModels.add(splitFriendModel);
                        }
                    }
                    setDataForFriendList();

                }


            } catch (Exception e) {
                e.printStackTrace();
                //  Utils.showWarn(SplitPaymentActivity.this, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });

    }

    double newtotal = 0.0;

    @SuppressLint("WrongConstant")
    private void setDataForFriendList() {
        layoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvSplitFriendlist.setHasFixedSize(true);
        rvSplitFriendlist.setLayoutManager(layoutManager);
        rvSplitFriendlist.setItemViewCacheSize(2);
        rvSplitFriendlist.setNestedScrollingEnabled(false);
        ((DefaultItemAnimator) rvSplit.getItemAnimator()).setSupportsChangeAnimations(false);
        ((DefaultItemAnimator) rvSplitFriendlist.getItemAnimator()).setSupportsChangeAnimations(false);
        rvSplit.setItemAnimator(null);
        rvSplitFriendlist.setItemAnimator(null);


        splitFriendListAdapter = new SplitFriendListAdapter(this, splitFriendModels);

        splitFriendListAdapter.setSplitFriendListner(new SplitFriendListner() {
            @SuppressLint("NewApi")
            @Override
            public void addFriend(int pos, SplitFriendModel splitFriendModel) {

                btnPayNow.setEnabled(true);


                splitFriendModel.setPrice("");
                afterSplitFriendsArray.add(splitFriendModel);
                newtotal = mainTotal / afterSplitFriendsArray.size();

                String totalInTwoDecimal = Utils.convertToTwoDecimal(newtotal).toString();

                for (SplitFriendModel splt : afterSplitFriendsArray)
                    splt.setPrice(totalInTwoDecimal);

                afterSplitFriendAdapter.notifyDataSetChanged();
            }

            @Override
            public void removeFriend(int pos, SplitFriendModel splitFriendModel) {


                for (int i = 0; i < afterSplitFriendsArray.size(); i++) {
                    if (afterSplitFriendsArray.get(i).getFirstName().equals(splitFriendModel.getFirstName())) {
                        afterSplitFriendsArray.remove(i);

                        newtotal = mainTotal / afterSplitFriendsArray.size();

                        String totalInTwoDecimal = Utils.convertToTwoDecimal(newtotal).toString();


                        for (SplitFriendModel splt : afterSplitFriendsArray)
                            splt.setPrice(totalInTwoDecimal);


                        afterSplitFriendAdapter.notifyDataSetChanged();

                        if (afterSplitFriendsArray.size() == 1) {
                            btnPayNow.setEnabled(false);
                        }
                    }
                }


            }
        });
        rvSplitFriendlist.setAdapter(splitFriendListAdapter);
    }

}
