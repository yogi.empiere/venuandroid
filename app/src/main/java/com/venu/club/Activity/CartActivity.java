package com.venu.club.Activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.venu.club.Adapter.CartAdapter;
import com.venu.club.App;
import com.venu.club.Controls.EditTextLight;
import com.venu.club.Interfaces.CartListner;
import com.venu.club.Model.CartModel;
import com.venu.club.Model.VenuAddons;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.venu.club.Utility.MyConstants.DEFAULT_PAGE_RECORDS;
import static com.venu.club.Utility.MyConstants.KEY_CART_MAP;
import static com.venu.club.Utility.MyConstants.KEY_CART_PAYMENT;
import static com.venu.club.Utility.MyConstants.KEY_TOTAL_PRICE;
import static com.venu.club.Utility.Utils.calculateVenuFee;

public class CartActivity extends AppCompatActivity {

    public ImageView ivBack, IvAdd, IvDelete;
    public Button btnProceedtoPayment, btnSplitPayment;
    int count = 1;
    public EditTextLight etTip;
    public CheckBox checkbox;
    public TextView TvItemCount, TvSubTotal, TvVenuFee, TvTotal, lblAddTip;
    public LinearLayout llBack;
    public RecyclerView rvCartItemlist;
    public RecyclerView.LayoutManager layoutManager;
    public CartAdapter cartAdapter;
    public ArrayList<CartModel> cartItemsArray;
    public CartActivity instance;
    public MySharedPref myShared;
    public JsonParserUniversal jParser;
    public RelativeLayout relAlldata, relButtons, relTip, relNoOrders;
    public double mainTotal = 0.0, subTotal = 0.0, total = 0.0, venuFee = 0.0, tipAmount = 0.0;

    public String venueId = "", temp_cart_id = "", all_quantity = "";
    ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        instance = this;
        myShared = new MySharedPref(getApplicationContext());
        jParser = new JsonParserUniversal();
        bindWidgetReference();
        bindWidgetEvents();
        getCartData(0);
    }

    private void bindWidgetEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnProceedtoPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payNow(false);
            }
        });

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSplitPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payNow(true);
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relTip.setVisibility(View.VISIBLE);
                    etTip.requestFocus();
                } else {
                    Utils.hideKeyboard(instance);
                    tipAmount = 0.0;
                    reloadTotal();
                    relTip.setVisibility(View.INVISIBLE);

                }
            }
        });

        lblAddTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkbox.setChecked(!checkbox.isChecked());
            }
        });

        etTip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence != null && charSequence.length() != 0) {
                    try {
                        tipAmount = Double.parseDouble(charSequence.toString());
                        reloadTotal();
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }else {
                    tipAmount = 0.0;
                    reloadTotal();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void bindWidgetReference() {

        ivBack = findViewById(R.id.ivBack);
        btnProceedtoPayment = findViewById(R.id.btnProceedtoPayment);
        btnSplitPayment = findViewById(R.id.btnSplitPayment);
        /*IvAdd = findViewById(R.id.IvAddItem);
        IvDelete = findViewById(R.id.IvDeleteItem);*/
        checkbox = findViewById(R.id.checkbox);
        etTip = findViewById(R.id.etTip);
        TvItemCount = findViewById(R.id.TvItemCountNo);
        TvSubTotal = findViewById(R.id.TvSubTotal);
        TvVenuFee = findViewById(R.id.TvVenuFee);
        TvTotal = findViewById(R.id.TvTotal);
        lblAddTip = findViewById(R.id.lblAddTip);
        rvCartItemlist = findViewById(R.id.rvCartItemlist);
        llBack = findViewById(R.id.llBack);
        relAlldata = findViewById(R.id.relAlldata);
        relNoOrders = findViewById(R.id.relNoOrders);
        relButtons = findViewById(R.id.relButtons);
        relTip = findViewById(R.id.relTip);
    }

    private void getCartData(int page) {

        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("size", DEFAULT_PAGE_RECORDS);
        postData.put("userId", myShared.getUserId() + "");


        String URL = Utils.convertToGetURL(Services.GET_ALL_CART_ITEMS, postData);

        new NetworkCall(instance, URL, true, response -> {
            try {


                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    JSONObject jData = jsonObject.getJSONObject("data");
                    setData(jData.getJSONArray("contentList"));


                }

            } catch (Exception e) {
                e.printStackTrace();
                // Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });


    }


    public void updateCart(int pos, int quauntity) {


        CartModel cm = cartItemsArray.get(pos);

        List<String> vaIDS = new ArrayList<String>();
        List<String> vaPrices = new ArrayList<String>();
        for (VenuAddons va : cm.venuAddons) {
            vaIDS.add(va.id + "");
            vaPrices.add(va.price + "");
        }
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", myShared.getUserId().toString());
        postData.put("menuItemId", cm.menu_item_id + "");
        postData.put("quantity", quauntity + "");
        postData.put("addOns", TextUtils.join(",", vaIDS));
        postData.put("addOnPrice", TextUtils.join(",", vaPrices));

        new NetworkCall(instance, Services.UPDATE_CART, postData,
                true, response -> {
            try {

                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    if (quauntity == 0) {
                        cartItemsArray.remove(pos);

                        if (App.cartCounter == 1) {
                            relAlldata.setVisibility(View.GONE);
                            relButtons.setVisibility(View.GONE);
                            relNoOrders.setVisibility(View.VISIBLE);
                            App.cartCounter = 0;
                        } else {
                            --App.cartCounter;
                        }


                        cartAdapter.notifyDataSetChanged();
                    } else {
                        cartItemsArray.get(pos).setQuantity(quauntity);
                        cartAdapter.notifyDataSetChanged();
                    }

                    reloadTotal();
                }


            } catch (Exception e) {
                e.printStackTrace();
                //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });

    }

    private void setData(JSONArray jCartItemsArray) {

        try {

            if (jCartItemsArray.length() == 0) {
                relAlldata.setVisibility(View.GONE);
                relButtons.setVisibility(View.GONE);
                relNoOrders.setVisibility(View.VISIBLE);
                return;
            } else {
                relNoOrders.setVisibility(View.GONE);
                relAlldata.setVisibility(View.VISIBLE);
                relButtons.setVisibility(View.VISIBLE);
            }

            cartItemsArray = new ArrayList<>();

            for (int i = 0; i < jCartItemsArray.length(); i++) {

                JSONObject jCartObj = jCartItemsArray.getJSONObject(i);

                CartModel cartModel = (CartModel) jParser.parseJson(jCartObj, new CartModel());

                if (jCartObj.has("addons")) {
                    JSONArray jAddons = jCartObj.getJSONArray("addons");

                    if (jAddons.length() > 0) {
                        for (int a = 0; a < jAddons.length(); a++) {
                            VenuAddons vAddons = (VenuAddons) jParser.parseJson(jAddons.getJSONObject(a), new VenuAddons());
                            cartModel.venuAddons.add(vAddons);
                        }
                    }
                }

                cartItemsArray.add(cartModel);
            }

            layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvCartItemlist.setHasFixedSize(true);
            rvCartItemlist.setLayoutManager(layoutManager);
            rvCartItemlist.setItemViewCacheSize(2);
            cartAdapter = new CartAdapter(this, cartItemsArray);
            rvCartItemlist.setAdapter(cartAdapter);

            App.cartCounter = cartItemsArray.size();


            cartAdapter.setCartListner(new CartListner() {
                @Override
                public void changeSubTotal(int pos, double price, int quantity) {
                    updateCart(pos, quantity);
                }

                @Override
                public void reconfigureTotal() {
                    reloadTotal();
                }
            });

            reloadTotal();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void payNow(boolean isSplit) {

        if (venueId.isEmpty()) {
            Utils.showToast(instance, "Please try after later");
            return;
        }
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", myShared.getUserId().toString());
        postData.put("venueId", venueId);
        postData.put("totalAmount", String.valueOf(total));
        postData.put("venuFee", String.valueOf(venuFee));
        postData.put("isSplit", isSplit ? "Yes" : "No");
        postData.put("TipAmount", (int) (tipAmount) + "");
        postData.put("quantity", all_quantity);

        postData.put("isCart", "1");
        postData.put("menuItemId", temp_cart_id);

        if (isSplit) {
            Intent i = new Intent(CartActivity.this, SplitPaymentActivity.class);
            i.putExtra(KEY_CART_PAYMENT, true);
            i.putExtra(KEY_CART_MAP, postData);
            startActivity(i);

        } else {
            Intent i = new Intent(CartActivity.this, CardListActivity.class);
            i.putExtra(KEY_CART_PAYMENT, true);
            i.putExtra(KEY_CART_MAP, postData);
            startActivity(i);
        }
    }

    public void reloadTotal() {
        subTotal = 0.0;
        if (cartItemsArray.size() > 0) {
            venueId = cartItemsArray.get(0).venuID + "";

            ArrayList<String> tempCartIdArray = new ArrayList<>();
            ArrayList<String> tempQtyArray = new ArrayList<>();
            for (CartModel cm : cartItemsArray) {

                tempCartIdArray.add(cm.temp_cart_id + "");
                tempQtyArray.add(cm.quantity + "");

                double tempMenuItemPrice = 0.0;
                String miPrice = cm.price;

                if (!miPrice.isEmpty() && !miPrice.equalsIgnoreCase("free")) {
                    tempMenuItemPrice += Double.valueOf(Utils.convertToTwoDecimal(miPrice));
                }

                subTotal += cm.quantity * tempMenuItemPrice;

                double tempAddonPrice = 0.0;

                for (VenuAddons venuAddon : cm.venuAddons) {
                    String adPrice = venuAddon.price;
                    if (!adPrice.isEmpty() && !adPrice.equalsIgnoreCase("free")) {
                        tempAddonPrice += Double.valueOf(Utils.convertToTwoDecimal(adPrice));
                    }
                    subTotal += cm.quantity * tempAddonPrice;
                }
            }

            temp_cart_id = android.text.TextUtils.join(",", tempCartIdArray);
            all_quantity = android.text.TextUtils.join(",", tempQtyArray);


            TvSubTotal.setText("$" + Utils.convertToTwoDecimal(subTotal + ""));

          //  venuFee = calculateVenuFee(subTotal);
            venuFee = calculateVenuFee(subTotal);
            TvVenuFee.setText("$" + Utils.addExtraZero(venuFee));
           // TvVenuFee.setText("$" + Utils.convertToTwoDecimal(venuFee + ""));

            if (tipAmount != 0.0) {
                subTotal = tipAmount + subTotal;
            }

            total = subTotal + venuFee;
            TvTotal.setText("$" + Utils.convertToTwoDecimal(total + ""));

        } else {
            subTotal = 0.0;
            venuFee = 0.0;
            total = 0.0;
            tipAmount = 0.0;
        }
    }


}
