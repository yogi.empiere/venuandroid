package com.venu.club.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.venu.club.R;
import com.venu.club.Utility.Network.Services;

public class EndUserLicenceActivity extends AppCompatActivity {
    public WebView mywebview;
    public ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_user_licence);
        mywebview = findViewById(R.id.webView);
        mywebview.loadUrl(Services.TERMS_AND_COND);

        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       onBackPressed();
                    }
                });
            }
        });
    }
}
