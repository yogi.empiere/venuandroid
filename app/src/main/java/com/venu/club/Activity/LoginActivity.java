package com.venu.club.Activity;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.venu.club.App;
import com.venu.club.Model.UserModel;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    public Button btnfb_login, btnGoogle_login, btn_guest;
    public TextView TvEndLicence, TvPrivacy;
    public LoginButton loginButton;
    public SignInButton googleSignInbutton;
    public CallbackManager callbackManager;
    public LoginActivity instance;
    public JsonParserUniversal jParser;
    public MySharedPref mShared;

    public GoogleSignInOptions gso;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;


    @Override
    public void onStart() {
        super.onStart();
        LoginManager.getInstance().logOut();

        signOut();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;
        jParser = new JsonParserUniversal();
        mShared = new MySharedPref(getApplicationContext());
        bindWidgetReference();
        bindWidgetEvents();

        /*if (!mShared.getBoolean(MySharedPref.IS_ALARM_SET)) {
            System.out.println("onCreate");
            Utils.scheduleAlarm(this);
            mShared.setBoolean(MySharedPref.IS_ALARM_SET, true);
        }*/
    }

    private void bindWidgetReference() {
        /*Setup FB code*/
        setUpFacebook();
        setUpGoogle();
        btnfb_login = findViewById(R.id.btnfb_login);
        btnGoogle_login = findViewById(R.id.btnGoogle_login);
        btn_guest = findViewById(R.id.btn_guest);
        TvEndLicence = findViewById(R.id.TvEndLicence);
        TvPrivacy = findViewById(R.id.TvPrivacy);


    }

    private void bindWidgetEvents() {
        btn_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShared.sharedPrefClear();
                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        TvPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, PrivacyPolicyActivity.class);
                startActivity(i);
                finish();
            }
        });
        TvEndLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, EndUserLicenceActivity.class);
                startActivity(i);
                finish();
            }
        });
        btnGoogle_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInToGoogle();
            }
        });

        btnfb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressFacebookButton();
            }
        });


    }

    private void setUpFacebook() {
        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.btnfb_loginOriginal);
        loginButton.setReadPermissions("public_profile", "email"/*,"user_friends"*/);
    }

    private void setUpGoogle() {

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleSignInbutton = findViewById(R.id.btnGoogle_loginOriginal);


    }

    public void pressFacebookButton() {
        loginButton.performClick();
        loginButton.setPressed(true);
        loginButton.invalidate();
        loginButton.registerCallback(callbackManager, mCallBack);
        loginButton.setPressed(false);
        loginButton.invalidate();
    }

    public void parseGoogleButton() {
        googleSignInbutton.callOnClick();
        googleSignInbutton.setPressed(true);
        googleSignInbutton.invalidate();

        googleSignInbutton.setPressed(false);
        googleSignInbutton.invalidate();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            handleGoogleSignInResult(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            handleGoogleSignInResult(null);
        }
    }

    private void signInToGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signInToGoogle]

    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        handleGoogleSignInResult(null);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        handleGoogleSignInResult(null);
                        // [END_EXCLUDE]
                    }
                });
    }

    public void handleGoogleSignInResult(@Nullable GoogleSignInAccount account) {


        if (account != null) {
            String email = account.getEmail();
            String name = account.getDisplayName();
            String socialId = account.getId();
            String profilePictureUrl = account.getPhotoUrl() == null ? "" : account.getPhotoUrl().toString();

            System.out.println("Photo URL Sagar --> " + profilePictureUrl);


            loginSocialCall(email, socialId, name, profilePictureUrl, false);
        }

    }

    public void loginSocialCall(String email, String socialID, String name,
                                String profilePictureURL, boolean isFacebook) {

        HashMap<String, String> postData = new HashMap<>();
        postData.put("fbId", socialID);
        postData.put("deviceToken", Utils.getDeviceToken());
        postData.put("name", name);
        postData.put("email", email);
        postData.put("accountType", isFacebook ? "Facebook" : "GooglePlus");
        postData.put("profilePictureURL", profilePictureURL);
        postData.put("groupId", MyConstants.USER_GROUP_ID);
        postData.put("deviceType", "android");

        new NetworkCall(instance, Services.SOCIAL_LOGIN, postData,
                true, response -> {
            try {

                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    UserModel userModel = (UserModel) jParser.parseJson(jsonObject.getJSONObject("data"), new UserModel());

                    saveAllDataToSharedPref(userModel);

                    Intent i = new Intent(instance,
                            TermsAndConditionActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    //new MySharedPref(instance).sharedPrefClear();
                    Utils.showWarn(LoginActivity.this, "Something went wrong, please try again.");
                }

            } catch (Exception e) {
                e.printStackTrace();

              //  Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
            }
        });

    }


    public void saveAllDataToSharedPref(UserModel loggedInUser) {
        mShared.setInt(mShared.USER_ID, loggedInUser.userId);
        mShared.setString(mShared.USER_EMAIL, loggedInUser.email);
        mShared.setString(mShared.USER_F_NAME, loggedInUser.firstName);
        mShared.setString(mShared.USER_L_NAME, loggedInUser.lastName);

        mShared.setString(mShared.USER_PROFILE_IMAGE, loggedInUser.imageUrl);
        mShared.setString(mShared.USER_ACCESS_TOKEN, loggedInUser.accessToken);
        mShared.setBoolean(mShared.USER_IS_FIRST_TIME, loggedInUser.isFirstTimeLogin);
        mShared.setInt(mShared.E_NOTIFY, loggedInUser.e_notify);

        App.loggedInUser = loggedInUser;

    }


    private FacebookCallback<LoginResult> mCallBack =
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(), (object, response) -> {
                                try {
                                    Log.i("social_fb", object.toString());
                                    String facebookID = object.getString("id");
                                    String email = object.getString("email");
                                    String name = object.getString("name");
                                    String gender = "Male";
                                    if (object.has("gender"))
                                        gender = object.getString("gender");

                                    gender = gender.toLowerCase();
                                    if (gender.equals("female"))
                                        gender = "Female";
                                    else
                                        gender = "Male";
                                    String image = "https://graph.facebook.com/" + facebookID + "/picture?type=large";


                                    //getFriends(facebookID);

                                    loginSocialCall(email, facebookID, name, image, true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,birthday,installed");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException e) {
                    e.printStackTrace();
                }
            };

}
