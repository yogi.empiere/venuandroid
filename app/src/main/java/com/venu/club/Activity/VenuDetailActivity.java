package com.venu.club.Activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.venu.club.Adapter.OpeningHourAdapter;
import com.venu.club.Controls.TextBold;
import com.venu.club.Controls.TextLight;
import com.venu.club.Model.OpeningHourModel;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;

public class VenuDetailActivity extends AppCompatActivity {
    public Button btnViewMenu;
    public ImageView ivBack, Ivdown, ivBar;
    public ImageBadgeView Ivcart;
    public LinearLayout llHours, llOpenToday, llBack, llDown;
    public RecyclerView rvHour;
    public TextView tvReview, tvStar, tvBarName, tvHeader, tvAddress, tvOpenToday;

    public TextView tvOpenTime;
    public RecyclerView.LayoutManager layoutManager;
    public OpeningHourAdapter openingHourAdapter;
    public ArrayList<OpeningHourModel> openingHourModels = new ArrayList<>();

    public JsonParserUniversal jParser;
    public VenuModel selectedVenu;
    public VenuDetailActivity instance;
    public int venuRatting = 0, venuReviews = 0;
    public String utcOffset = "", open_close_time = "";
    public boolean open_now = false, isAllDayVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venu_detail);
        instance = this;
        jParser = new JsonParserUniversal();
        selectedVenu = (VenuModel) getIntent().getSerializableExtra(KEY_VENU_MODEL);
        bindWidgetReference();
        bindWidgetEvents();

        if (!Utils.isDebug(instance))
            getPlaceDetail();
        setData();
    }

    private void bindWidgetReference() {
        btnViewMenu = findViewById(R.id.btnViewMenu);
        ivBack = findViewById(R.id.ivBack);
        Ivdown = findViewById(R.id.Ivdown);
        llHours = findViewById(R.id.llHours);
        llOpenToday = findViewById(R.id.llOpenToday);
        rvHour = findViewById(R.id.rvHour);
        Ivcart = findViewById(R.id.Ivcart);
        llBack = findViewById(R.id.llBack);
        llDown = findViewById(R.id.llDown);

        tvReview = findViewById(R.id.TvReview);
        tvStar = findViewById(R.id.TvStars);
        tvBarName = findViewById(R.id.tvBarName);
        tvHeader = findViewById(R.id.tvHeader);
        tvAddress = findViewById(R.id.tvAddress);
        tvOpenToday = findViewById(R.id.tvOpenToday);
        tvOpenTime = findViewById(R.id.TvOpenTime);
        ivBar = findViewById(R.id.ivBar);


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (new MySharedPref(getApplicationContext()).isLogin()) {
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }

    }

    private void bindWidgetEvents() {
        btnViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(VenuDetailActivity.this, MenuActivity.class);
                i.putExtra(KEY_VENU_MODEL, selectedVenu);
                startActivity(i);

            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        llDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maintainOpenDays();
            }
        });

        llHours.setVisibility(View.INVISIBLE);
        Ivdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maintainOpenDays();
            }
        });

        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(VenuDetailActivity.this, CartActivity.class);
                startActivity(i);
            }
        });
    }

    public void maintainOpenDays() {

        if (isAllDayVisible) {
            isAllDayVisible = false;
            Ivdown.setImageResource(R.drawable.ivdown);
            llHours.setVisibility(View.INVISIBLE);
            setData();
        } else {
            isAllDayVisible = true;
            Ivdown.setImageResource(R.drawable.ivarrowup);
            llHours.setVisibility(View.VISIBLE);
            setData();

        }

    }

    private void getPlaceDetail() {


        HashMap<String, String> postData = new HashMap<>();
        postData.put("placeid", selectedVenu.placeID);
        postData.put("key", getString(R.string.google_places_key));


        String URL = Utils.convertToGetURL(Services.MAP_BASE_URL, postData);

        new NetworkCall(instance, URL, true, response -> {
            try {

                if (response != null) {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jResultObj = jsonObject.getJSONObject("result");

                    if (jResultObj.has("rating"))
                        venuRatting = jResultObj.getInt("rating");


                    if (jResultObj.has("reviews"))
                        venuReviews = jResultObj.getJSONArray("reviews").length();


                    if (jResultObj.has("utcOffset"))
                        utcOffset = jResultObj.getString("utcOffset");


                    if (jResultObj.has("opening_hours")) {
                        JSONObject jOpeningHours = jResultObj.getJSONObject("opening_hours");

                        if (jOpeningHours.has("open_now"))
                            open_now = jOpeningHours.getBoolean("open_now");

                        if (jOpeningHours.has("periods")) {
                            JSONArray jPeriodsArray = jOpeningHours.getJSONArray("periods");

                            int currentDay = Utils.getCurrentDayOfWeek();
                            boolean gotToday = false;
                            String openTime = "", closeTime = "";

                            for (int i = 0; i < jPeriodsArray.length(); i++) {

                                JSONObject jSinglePeriod = jPeriodsArray.getJSONObject(i);

                                if (jSinglePeriod.has("open")) {

                                    JSONObject jOpen = jSinglePeriod.getJSONObject("open");

                                    int day = Integer.parseInt(jOpen.getString("day"));
                                    openTime = jOpen.getString("time");

                                    if (currentDay == day) {
                                        gotToday = true;
                                    } else {
                                        gotToday = false;
                                    }

                                }

                                if (jSinglePeriod.has("close")) {

                                    JSONObject jOpen = jSinglePeriod.getJSONObject("close");
                                    closeTime = jOpen.getString("time");
                                }

                                Log.i("Venu Time : ", openTime + " :: " + closeTime);

                                if (gotToday) {
                                    open_close_time = Utils.convertToAmPM(openTime) + " - " + Utils.convertToAmPM(closeTime);
                                }

                            }
                        }

                        if (jOpeningHours.has("weekday_text")) {
                            JSONArray jDaysArray = jOpeningHours.getJSONArray("weekday_text");

                            for (int i = 0; i < jDaysArray.length(); i++) {
                                OpeningHourModel op = new OpeningHourModel();
                                String day = jDaysArray.get(i).toString();

                                if (day.contains(":")) {
                                    String tempDay = day.substring(0, day.indexOf(":"));
                                    String tempTime = day.substring(day.indexOf(":") + 1, day.length());
                                    op.setDays(tempDay);
                                    op.setTime(tempTime);
                                } else {
                                    op.setDays(day);
                                }

                                openingHourModels.add(op);
                            }

                            setGoogleData();
                        }

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
                //   Utils.showWarn(instance, MyConstants.COMMON_ERROR_MESSAGE);
                setData();
            }
        });

    }


    private void setData() {
        tvReview.setText(venuReviews + "");
        tvStar.setText(venuRatting + " Of 5 Stars");
        tvBarName.setText(selectedVenu.getName());
        tvHeader.setText(selectedVenu.getName());
        tvAddress.setText(selectedVenu.getAddress());


        String ImageURL = "";
        if (selectedVenu.venuImagesArray.size() > 0) {
            ImageURL = Services.BASE_IMAGE_URL + selectedVenu.venuImagesArray.get(0).image_url;
        }

        Picasso.get()
                .load(ImageURL)
                .fit().centerCrop()
                .error(R.drawable.noimage)
                .into(ivBar);


    }

    public void setGoogleData() {
        tvOpenTime.setText(open_close_time);
        tvOpenToday.setText(open_now ? "Open Today" : "Closed Today");

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvHour.setHasFixedSize(true);
        rvHour.setLayoutManager(layoutManager);
        openingHourAdapter = new OpeningHourAdapter(this, openingHourModels);
        rvHour.setAdapter(openingHourAdapter);
    }

}
