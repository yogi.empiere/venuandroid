package com.venu.club.Activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Adapter.GooglePlacesAutocompleteAdapter;
import com.venu.club.Adapter.HomeAdapter;
import com.venu.club.Model.VenuModel;
import com.venu.club.Model.VenueImages;
import com.venu.club.R;
import com.venu.club.Utility.JsonParserUniversal;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.PlacesDetails;
import com.venu.club.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchActivity extends AppCompatActivity {
    public TextView tvcancel;
    public LinearLayout llNoSearchFound;
    public RecyclerView my_recycler_view;
    public AutoCompleteTextView etLocationAutoComplete;
    int maxlength = 3;

    public RecyclerView.LayoutManager layoutManager;
    public SwipeRefreshLayout swipeContainer;

    public ArrayList<VenuModel> venuArray;
    public JsonParserUniversal jParser;
    private double longitude = 0.0, latitude = 0.0;

    public MySharedPref myShared;

    private GooglePlacesAutocompleteAdapter mPlacesAdapter;
    public HomeAdapter homeAdapter;
    public ArrayAdapter<String> mAutoCompleteAdapter;
    public boolean isLocationSelected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        myShared = new MySharedPref(getApplicationContext());
        mPlacesAdapter = new GooglePlacesAutocompleteAdapter(SearchActivity.this, R.id.txt_places_name);
        jParser = new JsonParserUniversal();

        tvcancel = findViewById(R.id.tvcancel);
        llNoSearchFound = findViewById(R.id.llNoSearchFound);
        my_recycler_view = findViewById(R.id.my_recycler_view);
        etLocationAutoComplete = findViewById(R.id.etSeatch);
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(R.color.grey, R.color.grey_border, R.color.textcolor2, R.color.colorPrimaryDark);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNearByVenues(0);
            }
        });

        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(SearchActivity.this, HomeActivity.class);
                startActivity(I);
            }
        });


        etLocationAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {




                PlacesDetails placesDetails = new PlacesDetails();

                try {
                    JSONObject jsonObject = placesDetails.placeDetail(mPlacesAdapter.placeId.get(i));
                    longitude = jsonObject.getDouble("lng");
                    latitude = jsonObject.getDouble("lat");

                    System.out.println("-----longitude " + longitude);
                    System.out.println("-----latitude " + latitude);

                    fetchNearByVenues(0);

                } catch (JSONException e) {
                    e.printStackTrace();
                    llNoSearchFound.setVisibility(View.VISIBLE);
                    swipeContainer.setVisibility(View.GONE);
                    return;
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    llNoSearchFound.setVisibility(View.VISIBLE);
                    swipeContainer.setVisibility(View.GONE);
                    return;
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();

                    llNoSearchFound.setVisibility(View.VISIBLE);
                    swipeContainer.setVisibility(View.GONE);
                    return;
                }
            }
        });


        etLocationAutoComplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etLocationAutoComplete.isPerformingCompletion()) {
                    // An item has been selected from the list. Ignore.
                    return;
                }

                if (count == 0) {
                    if (venuArray != null)
                        venuArray.clear();
                    if (homeAdapter != null)
                        homeAdapter.notifyDataSetChanged();

                    swipeContainer.setVisibility(View.GONE);
                    llNoSearchFound.setVisibility(View.VISIBLE);
                    return;
                }


                    setAutoCompleteAdapter(mPlacesAdapter.autocomplete(s.toString()));
            }
        });

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
    }


    public void setAutoCompleteAdapter(ArrayList<String> data) {
        mAutoCompleteAdapter = new ArrayAdapter<String>(SearchActivity.this, R.layout.item_places_autocomplete, data);
        etLocationAutoComplete.setAdapter(mAutoCompleteAdapter);


    }


    private void setData() {
        isLocationSelected = true;
        layoutManager = new LinearLayoutManager(SearchActivity.this, RecyclerView.VERTICAL, false);
        my_recycler_view.setHasFixedSize(true);
        my_recycler_view.setLayoutManager(layoutManager);
        homeAdapter = new HomeAdapter(SearchActivity.this, venuArray);
        my_recycler_view.setAdapter(homeAdapter);
        Utils.hideKeyboard(SearchActivity.this);
    }


    private void fetchNearByVenues(int page) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("page", page + "");
        postData.put("lat", String.valueOf(latitude));
        postData.put("lon", String.valueOf(longitude));

        postData.put("size", MyConstants.DEFAULT_PAGE_RECORDS);


        String URL = Utils.convertToGetURL(Services.GET_VENUE_LIST_BY_LOCATION, postData);

        new NetworkCall(SearchActivity.this, URL, true, response -> {

            if (swipeContainer.isRefreshing())
                swipeContainer.setRefreshing(false);
            try {

                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    venuArray = new ArrayList<>();

                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    JSONArray jContentArray = jsonData.getJSONArray("contentList");

                    if (jContentArray.length() == 0) {
                        llNoSearchFound.setVisibility(View.VISIBLE);
                        swipeContainer.setVisibility(View.GONE);
                        return;

                    } else {
                        llNoSearchFound.setVisibility(View.GONE);
                        swipeContainer.setVisibility(View.VISIBLE);
                    }

                    for (int i = 0; i < jContentArray.length(); i++) {
                        JSONObject jVenuObject = jContentArray.getJSONObject(i);
                        VenuModel vm = (VenuModel) jParser.parseJson(jVenuObject, new VenuModel());
                        JSONArray jImagesArray = jVenuObject.getJSONArray("venueImages");

                        for (int j = 0; j < jImagesArray.length(); j++) {
                            JSONObject jImageObject = jImagesArray.getJSONObject(j);
                            VenueImages vi = (VenueImages) jParser.parseJson(jImageObject, new VenueImages());
                            vm.venuImagesArray.add(vi);
                        }

                        venuArray.add(vm);

                    }
                    setData();
                } else {
                    swipeContainer.setVisibility(View.GONE);
                    llNoSearchFound.setVisibility(View.VISIBLE);
                }


            } catch (Exception e) {
                e.printStackTrace();
                // Utils.showWarn(getActivity(), MyConstants.COMMON_ERROR_MESSAGE);
            }
        });
    }
}
