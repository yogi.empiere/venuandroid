package com.venu.club.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.venu.club.App;
import com.venu.club.Controls.EditTextLight;
import com.venu.club.Controls.TextLight;
import com.venu.club.R;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.venu.club.Utility.MyConstants.KEY_IS_FROM_TERMS;
import static com.venu.club.Utility.MyConstants.KEY_ORDER_ID;

public class ReportOrderActivity extends AppCompatActivity {

    public TextLight tvSubmit;
    public EditTextLight etMessage;
    public ReportOrderActivity instance;
    public String orderId = "";
    public MySharedPref myShared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_order);
        instance = this;
        myShared = new MySharedPref(getApplicationContext());
        if (getIntent() != null) {
            if (getIntent().hasExtra(KEY_ORDER_ID)) {
                orderId = getIntent().getStringExtra(KEY_ORDER_ID);
            }
        }

        tvSubmit = findViewById(R.id.tvSubmit);
        etMessage = findViewById(R.id.etMessage);


        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportOrder();
            }
        });

    }


    public void reportOrder() {
        String message = etMessage.getText().toString().trim();

        if (message.isEmpty()) {
            Utils.showToast(this, "Please enter valid reason for reporting this order");
            return;
        }


        HashMap<String, String> postData = new HashMap<>();
        postData.put("orderId", orderId);
        postData.put("userId", myShared.getUserId()+"");
        postData.put("reportedReason", message);

        new NetworkCall(instance, Services.REPORT_ORDER, postData,
                true, response -> {
            try {

                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    Utils.showWarn(instance, "Report has been sent to the Venu admin", 2, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            Intent i = new Intent(instance, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            instance.finish();
                            sweetAlertDialog.dismiss();
                        }
                    });
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
        });

    }
}
