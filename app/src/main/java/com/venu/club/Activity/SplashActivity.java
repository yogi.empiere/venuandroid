package com.venu.club.Activity;

import android.content.Intent;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.venu.club.App;
import com.venu.club.Model.UserModel;
import com.venu.club.R;
import com.venu.club.Utility.MySharedPref;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public MySharedPref mShared;
    public SplashActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        instance = this;

        mShared = new MySharedPref(getApplicationContext());

        if (App.loggedInUser == null) {
            if (mShared.isLogin()) {
                loadUserDataFromShared();
                redirectToScreen(true);
                return;
            }
        }

        redirectToScreen(false);

    }


    public void redirectToScreen(boolean isLoggedIn) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this,
                        isLoggedIn ? HomeActivity.class : LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    public void loadUserDataFromShared() {

        App.loggedInUser = new UserModel();
        App.loggedInUser.userId = mShared.getInt(mShared.USER_ID);
        App.loggedInUser.email = mShared.getString(mShared.USER_EMAIL, "");
        App.loggedInUser.firstName = mShared.getString(mShared.USER_F_NAME, "");
        App.loggedInUser.lastName = mShared.getString(mShared.USER_L_NAME, "");
        App.loggedInUser.thumbImageUrl = mShared.getString(mShared.USER_PROFILE_IMAGE, "");
        App.loggedInUser.imageUrl = mShared.getString(mShared.USER_PROFILE_IMAGE, "");
        App.loggedInUser.accessToken = mShared.getString(mShared.USER_ACCESS_TOKEN, "");
        App.loggedInUser.isFirstTimeLogin = mShared.getBoolean(mShared.USER_IS_FIRST_TIME);
        App.loggedInUser.e_notify = mShared.getInt(mShared.E_NOTIFY);


        Log.d("userid:::>", String.valueOf(mShared.getInt(mShared.USER_ID)));
        Log.d("username:::>", mShared.getString(mShared.USER_F_NAME, ""));

    }

}

