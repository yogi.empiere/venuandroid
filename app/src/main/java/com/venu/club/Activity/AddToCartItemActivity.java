package com.venu.club.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.App;
import com.venu.club.Model.UserModel;
import com.venu.club.Model.VenuAddons;
import com.venu.club.Model.VenuBrands;
import com.venu.club.Model.VenuModel;
import com.venu.club.R;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Network.NetworkCall;
import com.venu.club.Utility.Network.Services;
import com.venu.club.Utility.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Utility.MyConstants.KEY_ADDON_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_BRAND_MODEL;
import static com.venu.club.Utility.MyConstants.KEY_MENU_ITEM_NAME;
import static com.venu.club.Utility.MyConstants.KEY_VENU_CATEGORY_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MENU_ITEM_ID;
import static com.venu.club.Utility.MyConstants.KEY_VENU_MODEL;
import static com.venu.club.Utility.Utils.calculateVenuFee;


public class AddToCartItemActivity extends AppCompatActivity {
    public TextView tvcancel, TvItemCount, TvItemNo, tvBrandName, tvBrandPrice, tvHeader, tvSubTotal, tvTotal, tvVenuFee;
    public ImageView IvAdd, IvDelete;
    public ImageBadgeView Ivcart;
    public LinearLayout linAddons;
    public Button btnAddtocart;
    int quauntityCount = 1;
    private Dialog dialog;
    public VenuBrands vBrandModel;
    public VenuAddons vAddonModel;
    public int venuMenuItemId = 0;
    public Double brandPrice = 0.0, addonPrice = 0.0, subTotal = 0.0, total = 0.0, venuFee = 0.0;
    public MySharedPref mShared;
    private AddToCartItemActivity instance;
    public ArrayList<VenuAddons> venuAddonsArray = new ArrayList<>();
    public VenuModel selectedVenu;
    public String menuItemName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart_item);
        instance = this;
        mShared = new MySharedPref(getApplicationContext());
        bindWidgetReference();
        bindWidgetEvents();


        Intent mIntent = getIntent();

        if (mIntent.hasExtra(KEY_VENU_MENU_ITEM_ID)) {
            venuMenuItemId = mIntent.getIntExtra(KEY_VENU_MENU_ITEM_ID, 0);
        }

        if (mIntent.hasExtra(KEY_BRAND_MODEL)) {
            vBrandModel = (VenuBrands) mIntent.getSerializableExtra(KEY_BRAND_MODEL);
        }

        if (mIntent.hasExtra(KEY_ADDON_MODEL)) {
            venuAddonsArray = (ArrayList<VenuAddons>) mIntent.getSerializableExtra(KEY_ADDON_MODEL);
        }

        if (mIntent.hasExtra(KEY_VENU_MODEL)) {
            selectedVenu = (VenuModel) getIntent().getSerializableExtra(KEY_VENU_MODEL);

        }

        if (mIntent.hasExtra(KEY_MENU_ITEM_NAME)) {
            menuItemName = getIntent().getStringExtra(KEY_MENU_ITEM_NAME);

        }

        setData();
    }

    public void setData() {

        if (vBrandModel != null) {

            if (vBrandModel.brandName.equalsIgnoreCase("no brand") || vBrandModel.brandName.equalsIgnoreCase("nobrand")) {
                tvHeader.setText(menuItemName);
                tvBrandName.setText(menuItemName);
            } else {
                tvHeader.setText(vBrandModel.brandName);
                tvBrandName.setText(vBrandModel.brandName);
            }

            String brPrice = vBrandModel.price;


            if (!brPrice.isEmpty() && !brPrice.equalsIgnoreCase("free")) {
                brandPrice = Double.valueOf(Utils.convertToTwoDecimal(brPrice));
            }
            tvBrandPrice.setText("$" + Utils.convertToTwoDecimal(brandPrice + ""));

        }

        if (venuAddonsArray.size() == 0) {
            linAddons.setVisibility(View.GONE);
        }

        setTotal();

    }

    public void setTotal() {

        tvBrandPrice.setText("$" + Utils.convertToTwoDecimal((quauntityCount * brandPrice) + ""));

        addMultipleAddons();

        subTotal = (quauntityCount) * (brandPrice + addonPrice);
        tvSubTotal.setText("$" + Utils.convertToTwoDecimal(subTotal + ""));

        venuFee = calculateVenuFee(subTotal);
        tvVenuFee.setText("$" + Utils.addExtraZero(venuFee));

        total = subTotal + venuFee;
        tvTotal.setText("$" + Utils.convertToTwoDecimal(total + ""));


    }


    private void bindWidgetEvents() {
        IvDelete.setVisibility(View.GONE);

        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });
        btnAddtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShared.isLogin()) {
                    addToCartAPICall();
                } else {
                    Utils.showWarn(instance, "Please login first to buy a drink", 3, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            try {
                                Intent i = new Intent(instance, LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                                sweetAlertDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
        IvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quauntityCount = quauntityCount + 1;
                TvItemCount.setText(quauntityCount + "");
                TvItemNo.setText(quauntityCount + "");
                IvDelete.setVisibility(View.VISIBLE);
                setTotal();
            }
        });

        IvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("src", "Decreasing value... " + quauntityCount);
                if (quauntityCount > 0) {
                    quauntityCount = quauntityCount - 1;
                }
                if (quauntityCount == 1) {
                    IvDelete.setVisibility(View.GONE);
                } else if (quauntityCount > 1) {
                    IvDelete.setVisibility(View.VISIBLE);
                } else {
                    Log.d("src", "Value can't be less than 0");
                }

                TvItemCount.setText(quauntityCount + "");
                TvItemNo.setText(quauntityCount + "");
                setTotal();
            }
        });
        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartDialogue();
            }
        });
    }


    private void bindWidgetReference() {
        tvcancel = findViewById(R.id.cancel);
        IvAdd = findViewById(R.id.IvAdd);
        tvHeader = findViewById(R.id.tvHeader);
        Ivcart = findViewById(R.id.Ivcart);
        IvDelete = findViewById(R.id.IvDelete);
        TvItemCount = findViewById(R.id.TvItemCount);
        TvItemNo = findViewById(R.id.TvItemNo);
        tvBrandName = findViewById(R.id.tvBrandName);
        tvBrandPrice = findViewById(R.id.tvBrandPrice);
        tvSubTotal = findViewById(R.id.TvSubTotal);
        tvVenuFee = findViewById(R.id.TvVenuFee);
        tvTotal = findViewById(R.id.TvTotalPrice);


        btnAddtocart = findViewById(R.id.btnAddtocart);
        linAddons = findViewById(R.id.linAddons);


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mShared.isLogin()) {
            Utils.setCartBadge(Ivcart);
            Ivcart.setVisibility(View.VISIBLE);
        } else {
            Ivcart.setVisibility(View.GONE);
        }

    }

    private void createDialog() {


        dialog = new Dialog(AddToCartItemActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_delete);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        LinearLayout llCancelOrder = dialog.findViewById(R.id.llCancelOrder);
        LinearLayout llYes = dialog.findViewById(R.id.llYes);
        LinearLayout llNo = dialog.findViewById(R.id.llNo);
        llNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        dialog.show();
    }

    private void addToCartDialogue() {
        dialog = new Dialog(AddToCartItemActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_item_addtocart);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        LinearLayout llCancelOrder = dialog.findViewById(R.id.llAddtoCart);
        LinearLayout llOk = dialog.findViewById(R.id.llOk);
        llOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(instance, MenuActivity.class);
                //i.putExtra(KEY_VENU_MODEL, selectedVenu);
                //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //startActivity(i);
                dialog.dismiss();
                BrandAddonsActivity.mBrandAddonsActivity.finish();
                finish();
            }
        });
        dialog.show();
    }

    public void addToCartAPICall() {
        /*"userId", "menuItemId" = "1", "quantity" = "10", "addOns" = "1,2,3", "addOnPrice" = "25.00,27.00,30.00"
         */

        List<String> vaIDS = new ArrayList<String>();
        List<String> vaPrices = new ArrayList<String>();
        for (VenuAddons va : venuAddonsArray) {
            vaIDS.add(va.id + "");
            vaPrices.add(va.price + "");
        }
        HashMap<String, String> postData = new HashMap<>();
        postData.put("userId", mShared.getUserId().toString());
        postData.put("menuItemId", venuMenuItemId + "");
        postData.put("quantity", quauntityCount + "");
        postData.put("addOns", TextUtils.join(",", vaIDS));
        postData.put("addOnPrice", TextUtils.join(",", vaPrices));

        new NetworkCall(instance, Services.ADD_TO_CART, postData,
                true, response -> {
            try {

                Log.i("Response From Server :", response);
                JSONObject jsonObject = new JSONObject(response);
                boolean status = jsonObject.getBoolean("success");

                if (status) {
                    App.cartCounter++;
                    addToCartDialogue();
                } else {
                    //new MySharedPref(instance).sharedPrefClear();
                    Utils.showWarn(AddToCartItemActivity.this, "Something went wrong, please try again", 1);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }
        });

    }

    public void addMultipleAddons() {

        addonPrice = 0.0;
        linAddons.removeAllViews();
        linAddons.invalidate();

        for (VenuAddons va : venuAddonsArray) {

            View addOnsView = LayoutInflater.from(instance).inflate(
                    R.layout.q_sele_extra_addons, null);

            TextView tvAddonName, tvAddonPrice;

            tvAddonName = addOnsView.findViewById(R.id.tvAddonName);
            tvAddonPrice = addOnsView.findViewById(R.id.tvAddonPrice);

            tvAddonName.setText(va.getName());
            String adPrice = va.price;

            Double tempAddonPrice = 0.0;
            if (!adPrice.isEmpty() && !adPrice.equalsIgnoreCase("free")) {
                tempAddonPrice += Double.valueOf(Utils.convertToTwoDecimal(adPrice));
            }

            addonPrice += tempAddonPrice;

            tempAddonPrice = quauntityCount * tempAddonPrice;

            tvAddonPrice.setText(tempAddonPrice == 0.0 ? "Free" : "$" + Utils.convertToTwoDecimal(tempAddonPrice + ""));
            linAddons.addView(addOnsView);

        }


    }
}
