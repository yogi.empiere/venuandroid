package com.venu.club.Activity;

import android.Manifest;
import android.content.Intent;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.venu.club.Fragment.HomeFragment;
import com.venu.club.Fragment.NotificationFragment;
import com.venu.club.Fragment.OrderHistoryFragment;
import com.venu.club.Fragment.SettingsFragment;
import com.venu.club.R;
import com.venu.club.Utility.MyConstants;
import com.venu.club.Utility.MySharedPref;
import com.venu.club.Utility.Utils;

import ru.nikartm.support.ImageBadgeView;

import static com.venu.club.Utility.MyConstants.KEY_ORDER_HISTORY_REDIRECT;
import static com.venu.club.Utility.MyConstants.MY_PERMISSIONS_REQUEST_LOCATION;

public class HomeActivity extends AppCompatActivity {
    public static DrawerLayout dLayout;
    public static ImageView ivmenu, IvSearch;
    public static ImageBadgeView Ivcart;
    public static FragmentManager manager;
    private View navHeader, toolbar;
    private NavigationView navView;
    public static AppCompatActivity activity;
    public TextView TvnearBy, TvOrderHistory, Tvnotifications, Tvsettings, TvSignup;
    public static TextView TvItem;
    public HomeActivity instance;
    public static LinearLayout toolbarTop;
    public MySharedPref mShared;

    public static void replaceFragment(Fragment fragment, boolean doAddToBackStack) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        //activity.getSupportFragmentManager().executePendingTransactions();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        instance = this;
        bindWidgetReference();
        mShared = new MySharedPref(getApplicationContext());
        replaceFragment(new HomeFragment(), false);
        bindWidgetEvents();
        setNavigationDrawer();
        closeDrawer();

        if (getIntent() != null) {
            if (getIntent().hasExtra(KEY_ORDER_HISTORY_REDIRECT)) {
                closeDrawer();
                replaceFragment(new OrderHistoryFragment(), false);
            }
        }


        System.out.println("onCreate");
        if (!Utils.checkAlaramSecheudled(this)) {
            Log.i("", "Venue location udpate started");
            Utils.scheduleAlarm(this);
        } else {
            Log.i("", "Venue location udpate stoped");
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Log.i("Venue", "Venue --> Home acitivty on Back Pressed");
    }

    private void bindWidgetReference() {
        toolbarTop = findViewById(R.id.toolbar);
        dLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        IvSearch = findViewById(R.id.IvSearch);
        Ivcart = (ImageBadgeView) findViewById(R.id.Ivcart);
        TvItem = findViewById(R.id.TvItemhome);
        ivmenu = findViewById(R.id.ivmenu);
        TvSignup = findViewById(R.id.TvSignup);

        manager = getSupportFragmentManager();
        activity = HomeActivity.this;
        IvSearch.setVisibility(View.VISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        Ivcart.setVisibility(View.INVISIBLE);
        TvItem.setVisibility(View.VISIBLE);
        TvItem.setText("Nearby Venues");


    }

    private void bindWidgetEvents() {
        ivmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.openDrawer(GravityCompat.END);
                setNavigationDrawer();
            }
        });
        Ivcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(i);
            }
        });


    }


    private void setNavigationDrawer() {
        dLayout = findViewById(R.id.drawer_layout);
        navView = findViewById(R.id.navigation);
        navHeader = navView.getHeaderView(0);

        TvnearBy = navView.findViewById(R.id.TvnearBy);
        TvOrderHistory = navView.findViewById(R.id.TvOrderHistory);
        Tvnotifications = navView.findViewById(R.id.Tvnotifications);
        Tvsettings = navView.findViewById(R.id.Tvsettings);
        TvSignup = navView.findViewById(R.id.TvSignup);


        if (!mShared.isLogin()) {
            TvOrderHistory.setVisibility(View.GONE);
            Tvsettings.setVisibility(View.GONE);
            Tvnotifications.setVisibility(View.GONE);
            TvSignup.setVisibility(View.VISIBLE);

        } else {
            TvSignup.setVisibility(View.GONE);
        }


        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem != null && menuItem.getItemId() == android.R.id.home) {
                    if (dLayout.isDrawerOpen(Gravity.RIGHT)) {
                        dLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        dLayout.openDrawer(Gravity.RIGHT);
                    }
                }
                return false;
            }
        });

        TvOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                replaceFragment(new OrderHistoryFragment(), false);
            }
        });
        TvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        IvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(i);
                finish();
            }
        });
        Tvnotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                replaceFragment(new NotificationFragment(), false);
            }
        });
        TvnearBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                replaceFragment(new HomeFragment(), false);
            }
        });
        Tvsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                replaceFragment(new SettingsFragment(), false);
            }
        });
        TvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(i);
                finish();
            }
        });
    }


    public static void closeDrawer() {
        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static void unlockDrawer() {
        dLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public static void openDraer() {
        dLayout.openDrawer(GravityCompat.END);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        HomeFragment hFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
        if (hFragment != null && hFragment.isVisible()) {
            Log.i("TAG --->", "Home Fragment Visible on Request permission");
            hFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            Log.i("TAG --->", "Home Fragment Visible Not on Request permission");
        }

    }


}

